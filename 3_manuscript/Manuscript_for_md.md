---
title: Carbon emission trends and sustainability options in Austrian health care
author:
  - name: Ulli Weisz
    email: ulli.weisz@boku.ac.at
    affiliation: a
  - name: Peter-Paul Pichler
    email: pichler@pik-potsdam.de
    affiliation: b
  - name: Ingram S Jaccard
    email: jaccard@pik-potsdam.de
    affiliation: b
  - name: Willi Haas
    email: willi.haas@boku.ac.at
    affiliation: a
  - name: Sarah Matej
    email: sarah.matej@boku.ac.at
    affiliation: a
  - name: Florian Bachner
    email: Florian.Bachner@goeg.at
    affiliation: c
  - name: Peter Nowak
    email: Peter.Nowak@goeg.at
    affiliation: c
  - name: Helga Weisz
    email: weisz@pik-potsdam.de
    affiliation: b,d
    footnote: Corresponding Author
address:
    
  - code: a
    address: Institute of Social Ecology, University of Natural Resources and Life Sciences Vienna, Schottenfeldgasse 29, 1070 Vienna, Austria
  - code: b
    address: Social Metabolism and Impacts, Potsdam Institute for Climate Impact Research, Member of the Leibniz Association, PO Box 60 12 03, 14412, Potsdam, Germany
  - code: c
    address: Austrian Public Health Institute (Gesundheit Österreich GmbH), Stubenring 6, 1010 Vienna, Austria
  - code: d
    address: Department of Cultural History & Theory and Department of Social Sciences, Humboldt University Berlin, Unter den Linden 6, 10117, Berlin, Germany

journal: "Resources, Conservation and Recycling"
date: "2020-03-09"
bibliography: bibliography.bib
linenumbers: true
header-includes:
   - \usepackage{booktabs}
output:
  md_document:
    variant: gfm
    preserve_yaml: true
always_allow_html: true

---



# Abstract

Health care is one of the largest and fastest growing service sectors in
OECD countries and a significant contributor to climate change. Health
care is also indispensable for human well-being. It is therefore crucial
to understand how the health care sector can reduce its emissions
without undermining its service quality. We break down the carbon
emissions of Austrian health care in unprecedented detail over a decade
starting in 2005. We calculated the carbon footprints of Austrian health
care providers and further decomposed the emissions attributable to
hospitals, the largest health care provider in Austria. We estimated
detailed life cycle assessments of the carbon emissions attributable to
energy use, the use of selected pharmaceuticals and medical goods and
induced private travel.

The Austrian health carbon footprint amounted to 6.8 million tons of CO2
in 2014, a decline of 14% since 2005, mainly due to the rising shares of
renewables in the Austrian energy sector. Complementary calculations of
the carbon emissions from energy use by Austrian health care providers
confirm this finding. Goods purchased by hospitals, pharmaceuticals and
other medical non-durables stand out as especially large contributors to
the health carbon footprint. Carbon emissions attributable to induced
travel increased by 15%, indicating the need to better align planning of
health care provision with transport and spatial planning. Concluding,
we argue that many untapped possibilities for reducing the carbon
footprint of health care exist and propose six concrete steps towards
sustainable health care that are applicable to most industrial
countries.

# Introduction

The 2015 Paris agreement’s central goal to stabilize global mean
temperature at well below 2° Celsius above pre-industrial levels
requires rapid emission reductions and ultimately net-zero GHG emissions
in all sectors of the economy by 2050 (IPCC 2018). Service sectors have
long been neglected in climate change mitigation studies, but recently
there has been an increase in activity in this area.

Health care is one of the largest and fastest growing service sectors in
OECD countries (OECD 2017b) and a significant contributor to climate
change (HCWH and ARUP 2019; Pichler et al. 2019; Watts et al. 2018,
2019). Multiple international actors have called for efforts on climate
change mitigation in the health care sector to be stepped up (McMichael
et al. 2009; Watts et al. 2018, 2019, 2017; WHO 2018; WBG 2017).

To date, two international studies on the carbon footprints of health
care sectors exist (HCWH and ARUP 2019; Pichler et al. 2019), as well as
a number of national studies (Chung and Meltzer 2009; Eckelman and
Sherman 2016; Eckelman, Sherman, and MacNeill 2018; Malik et al. 2018;
Nansai et al. 2020; NHS England, SDU, and SEI 2009; SDU 2016; Wu 2019).
These studies show quite a wide variation in the share of emissions
associated with health care in a country’s total carbon footprint.
Across studies and countries the health carbon footprint varies between
2% and 10% of the national carbon footprint with an average share of
about 5% overall. The health care sector overall is the most carbon
intensive service sector in OECD countries (Pichler et al. 2019) and a
recent study has found that the global pharmaceutical industry is more
emission intensive in terms of operating revenue generated than the
automotive industry (Belkhir and Elmeligi 2019).

The health carbon footprint studies applied environmentally-extended
single or multiregional input-output analysis (MRIO) where health care
is treated as a final demand sector that purchases goods and services
from various production sectors, including the health care production
sector. The health carbon footprint is the sum of all global supply
chain emissions that occurred in the production of all goods and
services purchased by final demand for health care. MRIO approaches to
health carbon footprints are indispensable for comprehensive and
consistent accounts of the scale of global emissions and other
environmental pressures attributable to health care, and for
international comparisons. However, MRIO is currently not granular
enough to provide sufficient information about specific mitigation
options like comparing between alternative products and treatments.

To estimate GHG footprints on the level of individual products,
treatments, industries or locations, life cycle assessment (LCA) is the
most established approach. In the literature, LCAs have been performed
for specific pharmaceuticals (McAlister et al. 2016; Parvatker et al.
2019; Wernet et al. 2010) and associated industries (Belkhir and
Elmeligi 2019; Gao et al. 2019), medical products (Belboom et al. 2011;
Eckelman et al. 2012; McGain et al. 2010; Usubharatana and Phungrassami
2018; Thiel et al. 2015) and medical procedures (Brown et al. 2012;
Connor, Lillywhite, and Cooke 2010; MacNeill, Lillywhite, and Brown
2017; Venkatesh et al. 2016; Campion et al. 2012). Such detailed
analyses are indispensable for identifying alternative forms of health
care provision that are equally effective but less carbon intensive
(Eckelman and Sherman 2016).

The current coverage of LCAs assessing the GHG emissions of medical
products and services is limited. Environmental life cycle data are only
available for a few of the thousands of different products in use by
health care systems. Beside the large number of products, this is
largely due to corporate secrecy regarding data on pharmaceutical
synthesis, pricing and expenditures (De Soete et al. 2017; Parvatker et
al. 2019; Wernet et al. 2010). Life-cycle inventories, on which LCAs are
based, also suffer from methodological limitations, such as incoherent
definitions of system boundaries and functional units, data errors and
gaps, or truncation and aggregations biases. These limitations are well
known and documented in the literature. Especially when scaled up, they
lead to substantial uncertainties in LCA results (Reap et al. 2008;
Lenzen 2000; Majeau-Bettez, Strømman, and Hertwich 2011).

The two approaches can be applied in a complementary way to map out key
intervention points for climate change mitigation within the health care
sector (Pichler et al. 2019; SDU and ERM 2014, 2017).

First, we show the overall CO2 footprint of Austrian health care in
annual time series for the decade beginning in 2005 using the
environmentally-extended multi-regional input-output model (EE-MRIO)
Eora and health expenditure data from the OECD (OECD 2018). To add
nuance to the coarse resolution of the EE-MRIO results we break down the
hospital sector across major consumption categories. We then report
results of a bottom up assessment of the carbon footprints of core goods
and services directly purchased or induced by health care using LCA and
other bottom up approaches. These bottom up accounts comprise: 1) CO2
emissions associated with direct energy use in the health care sector
based on surveys and life-cycle inventories, 2) life cycle GHG emissions
of selected common pharmaceuticals as well as medical gloves as an
example of a widely used medical product, and 3) the induced CO2
emissions from private travel of staff, patients and visitors.

Concluding, we present a systematic overview of climate change
mitigation options for the health care system that do not compromise the
quality of health service provision but often have the potential to be
beneficial to public health.

# Materials and Methods

We apply the definition of the health care sector as determined in the
System of Health Accounts (SHA) (OECD, Eurostat, and WHO 2017), which
comprises all activities of health care (i.e., curing, caring,
promoting, and preventing), ancillary health services, supporting
services, and investments both publicly and privately financed. Wellness
and further public health relevant activities such as investments in
general public safety are not considered. The nine main health care
provider categories can be found in the SI (table S1). Their system
boundaries are described in detail in the SHA manual (OECD, Eurostat,
and WHO 2017).

Combining MRIO analysis with several bottom up approaches to account for
carbon emissions from individual products and services required the use
of multiple secondary data sources, all of which were originally
compiled for different purposes. These data sources differ in their
coverage of greenhouse gases, years and health care providers. An
overview of the relationships between the different sources is given in
a feature matrix in the SI (table S1). The matrix shows how the SHA
health care providers relate to the MRIO model and the breakdown of
individual goods and services covered by our bottom-up analysis.

The selection of goods and services considered in our study was informed
by results from previous research about carbon hot spots in health care
systems (SDU and ERM 2014, 2017) and balanced against data availability.

The following sections provide an overview of the applied methods and
address the most important limitations of the different approaches.
Detailed explanation, including emission factors, data sources,
additional tables and a deeper discussion can be found in the SI (SI
text file). All results are provided in a dedicated data file (SI data
file).

## Overall carbon footprint of Austrian health care

Austria’s total health carbon footprint was estimated using the full
version (v199.82) of the environmentally-extended multi-regional
input-output (EE-MRIO) model Eora (Lenzen et al. 2013, 2012) and the
same procedure as described in (Pichler et al. 2019). We focus on CO2
and exclude other Kyoto gases from our analysis, due to high model
uncertainty (Pichler et al. 2019). As a rough orientation, (Chung and
Meltzer 2009) estimate the share of CO2 in the total GHG footprint of
the US health care system at 80%. Health expenditure data were taken
from the OECD online data base (OECD 2018). These data are supplied to
the OECD by the Austrian national statistical office, and are classified
by expenditure category according to the internationally harmonized
System of Health Accounts (SHA) (OECD, Eurostat, and WHO 2017).

### Limitations

The principal aim of this study was to complement CO2 footprint results
from a global EE-MRIO model with several bottom up emission accounting
approaches that allow a more detailed picture of the CO2 emissions in
the Austrian health care sector. While it is fairly straightforward to
estimate annual time series footprints with the EE-MRIO model,
constructing the bottom up accounts is very labor intensive and
critically constrained by the limited availability of secondary data
sources. It was decided early in the study to provide three bottom up
snapshot estimates for the years 2005, 2010, and 2015. Even though Eora
covers the entire period considered from 2005 to 2015, we have decided
to drop the results for 2015 due to concerns regarding data quality.
Therefore, we show results up to 2014. This means that there is an
unavoidable inconsistency in the final year results for the bottom up
accounts (2015) and the EE-MRIO estimates (2014). This is regrettable,
but it should not have too much impact on the interpretability of the
results, as we do not expect the carbon footprints to change rapidly
from 2014 to 2015 (see SI section 2 for further discussion).

More generally, the most important limitations regarding the carbon
footprint calculations stem from unknown uncertainties in the
classification and completeness of Austrian health care expenditure
statistics, the sectoral resolution of the input-output model, the
uncertainty introduced when mapping the external expenditure data to the
MRIO model, and the reliability of its GHG emission extensions.

## CO2 emissions from direct energy use of major health care providers

Specific energy use data were compiled for four major health care
providers:

1)  Hospitals: all consumption of electricity, district heating, liquid
    and gaseous fossil fuels (includes fuels used for hospital owned
    vehicle fleets)
2)  Resident doctors (ambulatory health services): all consumption of
    electricity, district heating, liquid and gaseous fossil fuels in
    medical practices.
3)  Home health care services (ambulatory health services): fuel for
    home visits of mobile health care staff
4)  Ambulance services and patient transportation (ancillary health
    services): fossil fuel consumption for patient transport

Emission factors from Austrian official GHG reporting were then used to
calculate the CO2 emissions from this direct energy use (see SI section
4).

### Limitations

Due to limited data availability, we could not cover all providers of
health care (see table S1 in the SI). In the case of c) and d), the
estimates are restricted to the energy use for transport. As a
consequence, our results likely underestimate actual energy consumption
in these categories.

## Hospital carbon footprint breakdown

In order to further break down the carbon footprint of hospitals, we
multiplied Austrian hospital cost aggregates by supply chain CO2
emission intensities derived from the EE-MRIO model Eora. Hospital cost
data are sourced from the official but non-public Austrian Health System
Statistics (BMASGK 2018) which covers all Austrian public hospitals.
These represent about 95% of all acute care services of the Austrian
hospital sector in terms of final costs. These data include aggregate
cost groups from which we excluded all costs assumed not to contribute
to the carbon footprint (mainly personnel costs) and investment costs to
avoid double counting. Additionally, we received data on selected
sub-groups, which we considered separately (i.e., pharmaceuticals, food,
cleaning agents, paper and office supply, and outsourced services:
laundry, cleaning, maintenance). In figure @ref(fig:fig2) the categories
have been summarized as follows:

  - Pharmaceuticals
  - Medical G\&S: other medical non-durable goods, medical minor value
    durables, medical services (outsourced)
  - Other G\&S: other non-medical goods (non-durables, minor value
    durables), other non-medical services (outscourced)
  - Food & Catering: food, catering
  - Facility Services: cleaning agents, office supply, paper, and
    outsourced services (laundry, cleaning, and maintenance)

CO2 emissions caused by direct energy use of hospitals (district
heating, electricity, oil and gas) were taken from the calculations
provided in the ‘direct energy use’ section above.

### Limitations

Due to limited data availability, we had to rely on top-down derived
aggregated average CO2 emission intensities (kgCO2/Euro).
Inconsistencies between OECD expenditure data and hospital cost
statistics could not be resolved entirely. Thus, uncertainties remain
and the contribution of the major consumption categories in hospitals to
the hospitals overall carbon footprint should be regarded to indicate
the magnitude of effects of major consumption categories rather than
providing robust absolute results.

## Selected pharmaceuticals and medical gloves

In the selection of the product examples, we considered findings from a
diverse body of literature addressing different aspects of medical
procurement, including climate relevance, indication of overuse,
existence of alternatives and intervention options and evidence on high
consumption in Austria (for details see SI, section 5).

We included anesthetic gases, pressurized metered dose inhalers (pMDI),
four simple chemically synthesized active pharmaceutical ingredients
(APIs; i.e., paracetamol, ibuprofen, acetylsalicylic acid, and naproxen)
used in painkillers, antibiotics and medical gloves (nitrile and latex
gloves) in our assessment. We used different data sources to estimate
the g CO2e/g carbon intensities (LCA-factors from Ecoinvent (v3.2) data
base for product components, and literature) and the overall yearly GHG
emissions of the individual examples. To that end, we used consumption
data published by (UBA 2016), and from hospitals (source “HA”:
confidential information from Austrian hospital cost accounts
representing about 14% of Austrian public hospitals in terms of final
costs, which we scaled up to all hospitals).

On-site GHG emissions from anesthetic gases were converted to CO2e using
their respective global warming potential (GWP). GHG emissions from the
use of pMDI in Austria were obtained from UNFCCC database (UNFCC 2018).
In the case of the four painkiller APIs, we considered the core
production process of the APIs by employing a stoichiometric approach
(Parvatker et al. 2019). The estimates of antibiotics are restricted to
the direct energy use during production as provided by (Sandoz GmbH
2017). To estimate the carbon footprint of medical gloves, we followed
the approach suggested by (SDU and ERM 2017) and attributed LCA factors
from the Ecoinvent database to the main components and production
process of the two types of gloves. Emissions are estimated in CO2e,
either because the GHG emissions associated with those goods are non-CO2
GHG gases (i.e., anesthetic gases, pDMI) or the LCA-factors were not
available for CO2 emissions. Depending on data availability, results are
presented for different years and either for Austrian hospitals (public
and private) or the entire Austrian health care sector.

### Limitations

Available LCA factors are based on different system boundaries, which is
a common problem in the LCA literature (Reap et al. 2008). Thus, it was
not feasible to carry out LCA-based bottom up calculations across the
individual products in a methodologically consistent manner.
Consequently, results are not directly comparable but give an indication
of the magnitude of the effects. Additionally, our stoichiometric
approach underestimates the GHG footprints of the pharmaceuticals
containing the APIs because it does not include additives and packaging.

## Private travel related CO2 emissions induced by the health care sector

Induced travel emissions are not part of the health care sector as
defined by SHA, and are therefore not included in the MRIO-based health
carbon footprint (see table S1). For calculating the CO2 emissions from
health care induced private travel, we estimated distances traveled
through commuting of health care staff as well as patient and visitor
travel to hospitals and providers of ambulatory health care (medical
practices).

These estimates were combined with modal split information and
mode-specific emission factors to determine the total direct emissions
from induced private transport.

### Limitations

No travel data were available for residential long-term care, ancillary
health services, health administration, medical retail and private
patients (not state-insured treatments). Also, no corrections could be
made for trips that were not exlusively for health related purposes
(e.g. trips comined with shopping).

# Results and Discussion

``` r
aut_summary = read_excel(file_results, sheet = "HCF_summary") 

aut_summary_endyear = aut_summary %>%
  filter(Year == final_year) 
aut_hcf_endyear_mt = round(aut_summary_endyear$hcf_kt*0.001,digits = 1)
aut_hcf_share_endyear = round(aut_summary_endyear$hcf_kt/aut_summary_endyear$national_cf_kt*100)
aut_hcf_pc_endyear_t = round(aut_summary_endyear$hcf_t_per_capita, digits=1)
aut_hcf_pe_endyear_kg = round(aut_summary_endyear$hcf_kt/aut_summary_endyear$hexp_const_2010_million_EUR, digits = 2)

## for graph abstract 
aut_summary_shareyear = aut_summary %>%
  filter(Year == share_year) 
aut_hcf_shareyear_mt = round(aut_summary_shareyear$hcf_kt*0.001,digits = 1)
aut_hcf_share_shareyear = round(aut_summary_shareyear$hcf_kt/aut_summary_shareyear$national_cf_kt*100)
aut_hcf_pc_shareyear_t = aut_summary_shareyear$hcf_t_per_capita
aut_hcf_pe_shareyear_kg = round(aut_summary_shareyear$hcf_kt/aut_summary_shareyear$hexp_const_2010_million_EUR, digits = 2)
### end graph abstract





aut_hcf_trend = as.integer(round((1-aut_summary[aut_summary$Year==final_year, "hcf_kt"]/
                         aut_summary[aut_summary$Year==2005, "hcf_kt"])*100))
aut_hce_trend = as.integer(round((aut_summary[aut_summary$Year==final_year, "hexp_const_2010_million_EUR"]/
                         aut_summary[aut_summary$Year==2005, "hexp_const_2010_million_EUR"]-1)*100))

dat_fig2 = read_excel(file_results, sheet = "Fig2_data") %>%
  filter(Year == share_year) 

percentages = dat_fig2 %>%
  group_by(Provider) %>%
  summarise(ktCO2 = sum(ktCO2)) %>%
  mutate(percent = round(ktCO2/sum(ktCO2)*100))

share_hospitals = percentages[percentages$Provider=="Hospitals", c("percent")]
share_ambulatory = percentages[percentages$Provider=="Ambulatory", c("percent")]
share_retail = percentages[percentages$Provider=="Medical Retail", c("percent")]
share_other = percentages[percentages$Provider=="Other", c("percent")]
share_investment = percentages[percentages$Provider=="Investment", c("percent")]
```

The Austrian health carbon footprint was `6.8` megatons of CO2 emissions
in `2014`, or `7`% of Austria’s national carbon footprint. This
corresponds to `0.8` tons of CO2 per capita and `0.2` kg per Euro of
health expenditure. In a comparative study of health carbon footprints,
Austria ranked 6th in terms of the share of health care emissions in the
total national carbon footprint among mostly OECD countries (Pichler et
al. 2019). The Austrian health carbon footprint was `14`% lower in
`2014` compared to 2005. During the same period, health expenditure in
real terms has increased by `20`% (figure @ref(fig:fig1)) and the carbon
intensity of the health care system (kgCO2/Euro health expenditure) was
reduced by half (see SI data file).

``` r
knitr::include_graphics(normalizePath(paste0(here(),"/figures/","figure1.pdf")))
```

<div class="figure" style="text-align: center">

<img src="/home/pichler/gits/carbon-emission-trends-and-sustainability-options-in-austrian-health-care/figures/figure1.pdf" alt="Austrian health carbon footprint by provider (left) and health care expenditure (right) from 2005 to 2014." width="100%" />

<p class="caption">

Austrian health carbon footprint by provider (left) and health care
expenditure (right) from 2005 to 2014.

</p>

</div>

To put these results in context, Austria’s total territorial CO2
emissions declined after a peak in 2005 from 79 MtCO2 to 64 MtCO2 in
2014 (UBA 2019). For the most part, this downward trend can be ascribed
to a rising market share of renewable energy and a shift from coal to
gas in the provision of electricity and thermal energy (Le Quéré et al.
2019; UBA 2018). The GHG emissions from the Austrian energy sector (98%
of which are CO2 emissions, (UBA 2019, p 55)) declined by 13% (from 67
MtCO2e to 52 MtCO2e) over this period. This suggests that the observed
emission reductions in the Austrian health care sector are primarily due
to improvements in the carbon efficiency of the energy sector in Austria
and elsewhere (Pichler et al. 2019). Unfortunately, the total
territorial emissions in Austria have increased again since. Between
2014 and 2017 emissions have increased from 64 MtCO2 to 70 MtCO2 (UBA
2019, p18). If the Austrian health carbon footprint has continued to
follow the national trend in recent years, it is therefore likely to be
higher today than it was in 2014.

A disaggregation of the health carbon footprint by SHA health care
providers in `2010` shows that hospitals account for the largest share
(`32`%), followed by retailers of medical goods and pharmaceuticals
(`20`%), ambulatory health care with (`18`%), and investments (`9`%).
Other providers together account for `20`% of the CO2 emissions caused
by health care, of which long-term care contributed the largest share
(figure @ref(fig:fig2), inner circle). These shares have essentially
remained stable over the observed period.

``` r
knitr::include_graphics(normalizePath(paste0(here(),"/figures/","figure2.pdf")))
```

<div class="figure" style="text-align: center">

<img src="/home/pichler/gits/carbon-emission-trends-and-sustainability-options-in-austrian-health-care/figures/figure2.pdf" alt="CO2 footprint of the Austrian health care sector in 2010 divided into SHA categories for health care providers and investments (inner ring). Minor categories are summarized into Other (section 2.1). In the categories Ambulatory and Hospitals (outer ring), emissions from direct energy consumption and induced travel (outer arc) are also reported (sections 2.2 and 2.5). The category Hospitals was further subdivided by consumed goods and services (section 2.3)" width="95%" />

<p class="caption">

CO2 footprint of the Austrian health care sector in 2010 divided into
SHA categories for health care providers and investments (inner ring).
Minor categories are summarized into Other (section 2.1). In the
categories Ambulatory and Hospitals (outer ring), emissions from direct
energy consumption and induced travel (outer arc) are also reported
(sections 2.2 and 2.5). The category Hospitals was further subdivided by
consumed goods and services (section 2.3)

</p>

</div>

In the following sections we break down the carbon footprint of the
major health care providers into more tangible consumption categories
using different bottom-up approaches (this is equivalent to saying that
we discuss the vertical axis of the feature matrix shown in table S1 in
SI).

## Major consumption categories in hospitals

``` r
dat_fig2 = read_excel(file_results, sheet = "Fig2_data") %>%
  filter(Year == share_year) %>%
  group_by(Provider) %>%
  mutate(percent = round(ktCO2/sum(ktCO2)*100))

hospitals_hcf_Mt = dat_fig2 %>%
  group_by(Provider) %>%
  summarise(ktCO2 = sum(ktCO2)) %>%
  filter(Provider == "Hospitals") %>%
  select(ktCO2) %>%
  transmute(MtCO2 = round(ktCO2*0.001, digits = 1))
  
hospitals_hcf_tCO2_pb = dat_fig2 %>%
  group_by(Provider) %>%
  summarise(ktCO2 = sum(ktCO2)) %>%
  filter(Provider == "Hospitals") %>%
  select(ktCO2) %>%
  transmute(tCO2_per_bed = round(ktCO2*1000/51745))


hospital_categories = dat_fig2 %>%
  group_by(Provider) %>%
  summarise(n = n()) %>%
  filter(Provider == "Hospitals") %>%
  select(n)


hosp_medic_share = dat_fig2[dat_fig2$Provider=="Hospitals" &
                              dat_fig2$Category=="Medical G&S", "percent"]
hosp_energ_share = sum(dat_fig2[dat_fig2$Provider=="Hospitals" &
                              dat_fig2$Category %in% c("Energy Fossil", 
                                                       "Energy Electricity", 
                                                       "Energy Dist. Heat"), "percent"])
hosp_pharm_share = dat_fig2[dat_fig2$Provider=="Hospitals" &
                              dat_fig2$Category=="Pharmaceuticals", "percent"]
```

With around `2.5` MtCO2 in `2010`, hospitals contributed the largest
share (`32`%) of CO2 emissions among all health care providers (Figure
@ref(fig:fig2) inner circle) to the health carbon footprint. With 51,745
total hospital beds in Austria (BMASGK 2019), this translates to around
`48` tCO2 per hospital bed and year. A breakdown of `8` consumption
categories (Figure @ref(fig:fig2), outer circle hospitals) shows that
the carbon footprint of hospitals is dominated by purchases of medical
goods and services (`36`%) and pharmaceuticals (`19`%) on the one hand,
and the consumption of energy services (`31`%) on the other hand.
Non-medical goods, outsourced non-medical services and food play a minor
role (see figure S1 in the SI).

Hospital-based care is the most costly and CO2 intensive form of health
care provision. Emission reduction efforts by hospitals often focus on
savings related to the direct energy use in areas that are not health
care specific, e.g., by taking action on insulation, heating, cooling
and ventilation (McGain and Naylor 2014; Tomson 2015). The emission
reduction potential associated with medical treatment and the use of
medical goods/pharmaceuticals largely remains untapped in the existing
literature on sustainable health care (Cimprich et al. 2019; McGain and
Naylor 2014). Our results indicate that effective climate mitigation
strategies of hospitals should be expanded to include the core medical
areas. Obvious intervention options address ecological procurement
(i.e., choosing low carbon product alternatives) and avoidance of
overconsumption (Cassel 2012). More far reaching options could consider
health care provision planning like avoiding misallocation in hospitals
(Weisz et al. 2011) and by reducing unnecessary hospital stays.

Austria has one of the highest hospitalization rates, number of hospital
beds and length of hospital stays in the EU. Many hospital admissions
are avoidable and amenable to primary care. One reason for unnecessary
hospital visits in Austria is the weak outpatient primary care sector
mainly provided by contracted solo practitioners. Also the number of day
surgeries is comparatively low (Bachner, Bobek, Habimana, et al. 2018).
Diabetes, chronic obstructive pulmonary disease (COPD) and congestive
heart failure show hospitalization rates above OECD average, despite
robust evidence of the conditions for effective treatment in primary
care (OECD 2017b). Reducing these forms of overprovision by shifting
unnecessary hospital-based health care to less expensive and less carbon
intensive forms of health care provision holds significant cost savings
for the health care sector (OECD 2017a) and a yet unexplored potential
for reducing the health carbon footprint.

Particularly in the case of food, other GHGs (methane and nitrous oxide)
contribute substantially to the overall GHG footprint (Vermeulen,
Campbell, and Ingram 2012). However, the climate effect caused by the
consumption of food and catering in the health care sector is small,
even if all Kyoto gases are considered (NHS and SDU 2012, 2009). The
relevance of food in the context of health care and climate change lies
rather in the opportunity to promote healthy and low carbon diets (Watts
et al. 2019; Willett et al. 2019).

## Emissions from direct energy use across health care providers

``` r
aut_summary = read_excel(file_results, sheet = "HCF_summary") 
aut_summary_year = aut_summary %>%
  filter(Year == share_year) 

dat_fig3 = read_excel(file_results, sheet = "Fig3_data") 
dat_fig3_sum = dat_fig3 %>%
  group_by(Year) %>%
  summarise(ktCO2 = sum(ktCO2)) %>%
  arrange(Year)

energy_total_2005 = round(sum(dat_fig3_sum$ktCO2[1]))
energy_total_2010 = round(sum(dat_fig3_sum$ktCO2[2]))
energy_total_2015 = round(sum(dat_fig3_sum$ktCO2[3]))

energy_share = round(sum(dat_fig3_sum[dat_fig3_sum$Year==share_year,"ktCO2"])/aut_summary_year$hcf_kt*100)

dat_fig3_share = dat_fig3 %>%
  group_by(Year) %>%
  mutate(percent = ktCO2/sum(ktCO2)*100) %>%
  group_by(Year, Provider) %>%
  summarise(percent = sum(percent)) %>%
  group_by(Provider) %>%
  summarise(percent = round(mean(percent)))

mean_share_ambu = dat_fig3_share[dat_fig3_share$Provider =="Ambulatory", "percent"]
mean_share_home = dat_fig3_share[dat_fig3_share$Provider =="Home health care services", "percent"]
mean_share_hosp = dat_fig3_share[dat_fig3_share$Provider =="Hospitals", "percent"]
mean_share_pati = dat_fig3_share[dat_fig3_share$Provider =="Patient Transportation", "percent"]

trend_hosp = dat_fig3 %>%
  group_by(Year, Provider) %>%
  summarise(ktCO2 = sum(ktCO2)) %>%
  filter(Provider == "Hospitals")

trend_hosp = round(100-trend_hosp[trend_hosp$Year==2015, "ktCO2"]/
  trend_hosp[trend_hosp$Year==2010, "ktCO2"]*100)

trend_patient = round(100-dat_fig3[dat_fig3$Year==2015 & dat_fig3$Provider=="Patient Transportation", "ktCO2"]/
  dat_fig3[dat_fig3$Year==2005 & dat_fig3$Provider=="Patient Transportation", "ktCO2"]*100)

trend_home = round(dat_fig3[dat_fig3$Year==2015 & dat_fig3$Provider=="Home health care services", "ktCO2"]/
  dat_fig3[dat_fig3$Year==2005 & dat_fig3$Provider=="Home health care services", "ktCO2"]*100-100)

#ambulances reduced CO2 emissions by 20% between 2005
```

The use of energy by health care providers causes on-site CO2 emissions
from burning petrol and diesel for transportation services (ambulance
and mobile health care) and oil and gas for heating. It also causes
supply chain emissions from electricity use, district heating and the
provision of the purchased fossil fuels (UBA 2017). These emissions
amounted to `949`, `924`, and `768` ktCO2 in the years 2005, 2010, and
2015, respectively and accounted for around `12`% of the Austrian health
carbon footprint in `2010`.

The average contribution of each main provider across the study period
was `83`% for hospitals, `12`% for providers of ambulatory health
services (including medical practices and home health care services)
`5`% for ancillary health services (ambulances and patient
transportation) (figure @ref(fig:fig3)). Hospitals are the most energy
and CO2 intensive form of health care provision; they operate 24 hours a
day and are more capital intensive than other health care providers,
because they require a lot of floor space to host patients, operate
laboratories, and specialized diagnostic and treatment facilities. As a
consequence, the average CO2 footprint from energy use per health
expenditure is 10 times higher in hospitals compared to ambulatory
health care.

However, since hospitals contribute more than 80% of the CO2 emissions
associated with direct energy use by the health care sector, the
mitigation potential is also substantial. In the context of climate
change, the provision of thermal services is a crucial element. Over the
study period hospitals achieved the largest CO2 emission reductions from
energy use, especially from 2010 to 2015 (`18`%), while CO2 emissions
from ambulatory health care remained stable (figure @ref(fig:fig3)). The
decrease in CO2 emissions from energy use indicates a shift from oil
heating to district heating in hospitals, but also reflects a general
decarbonization trend in Austria’s power sector as discussed above.

Austria has had several warm winters recently which have translated to
fewer heating degree days (minus 15% from 2005 to 2015 (UBA 2018)) and
consequently a lower energy demand for heating. As this trend continues,
the energy demand for heating will continue to decrease while at the
same time the demand for cooling, especially in hospitals, will
increase.

Another factor contributing to the overall decline of health care
emissions associated with direct energy use likely results from changes
in Austrian health policy. To reduce costs, Austrian health policy has
aimed to shift its extremely hospital-centered health care system from
acute care in hospitals to residential long-term care and ambulatory
health care provision (Bachner, Bobek, Lepuschütz, et al. 2018).
Quantifying the concrete emission reductions of these measures, however,
would require access to detailed performance and consumption data of all
health care providers and is beyond the scope of this study.

The development of emissions we find in the ambulatory sector is in line
with the expected results of such a shift in policy. Despite the
decarbonization trend in the power sector the emissions of ambulatory
health care across the study period remained more or less stable due to
an increasing number of residential doctors and rising demand in
ambulatory health services (Bachner, Bobek, Habimana, et al. 2018).

In addition, the CO2 emissions from patient transportation were `16`%
lower in 2015 compared to 2005 while during the same time emissions from
home health care grew `21`% due to increasing demand in services,
resulting in higher mileage.

``` r
knitr::include_graphics(normalizePath(paste0(here(),"/figures/","figure3.pdf")))
```

<div class="figure" style="text-align: center">

<img src="/home/pichler/gits/carbon-emission-trends-and-sustainability-options-in-austrian-health-care/figures/figure3.pdf" alt="Energy consumption of major Austrian health care providers: CO2 emissions 2005, 2010 and 2015" width="85%" />

<p class="caption">

Energy consumption of major Austrian health care providers: CO2
emissions 2005, 2010 and 2015

</p>

</div>

## Pharmaceuticals and other medical goods

``` r
dat_fig2 = read_excel(file_results, sheet = "Fig2_data") %>%
  filter(Year == share_year) %>% 
  mutate(percent = ktCO2/sum(ktCO2)*100)

aut_hcf = sum(dat_fig2$ktCO2)

dat_fig2 = dat_fig2 %>%
  filter(Provider %in% c("Hospitals", "Medical Retail")) %>% 
  filter(Category %in% c("Pharmaceuticals", "Medical G&S", "Other medical")) %>%
  group_by(Category) %>%
  summarise(ktCO2 = sum(ktCO2),
            percent = sum(percent))

ph_and_medical_percent = round(sum(dat_fig2$percent))
ph_and_medical_total = round(sum(dat_fig2$ktCO2)*0.001)

pharmaceuticals_share = round(sum(dat_fig2[dat_fig2$Category=="Pharmaceuticals", "percent"]))
othermedical_share = ph_and_medical_percent - pharmaceuticals_share

ph_med_in_hcf_share = round(ph_and_medical_total/aut_hcf*100)
```

The largest contribution to the health carbon footprint across health
care providers is from pharmaceuticals and other medical goods (`38`%)
provided in hospitals and through medical retail (figure
@ref(fig:fig2)). Pharmaceuticals and other medical goods are comprised
of thousands of individual products. In general, data on their use and
the resulting carbon emissions are scarce. In Austria, there are about
10,000 registered pharmaceuticals (BASG 2019) that are directly used in
medical treatment. About `3` MtCO2 emissions result from the use of
pharmaceuticals (`21`% of HCF) and other medical goods (`17`% of HCF).

``` r
#options(knitr.table.format = "latex")

dat_tab1 = read_excel(file_results, sheet = "Tab1_data")

naproxen_intensity = dat_tab1 %>%
  filter(`API.Name` == 'Naproxen') %>%
  select(`gCO2e_per_gAPI`) %>%
  as.numeric()

paracetamol_intensity = dat_tab1 %>%
  filter(`API.Name` == 'Paracetamol') %>%
  select(`gCO2e_per_gAPI`) %>%
  as.numeric()

paracetamol_total = dat_tab1 %>%
  filter(`API.Name` == 'Paracetamol') %>%
  select(`ktCO2e_per_year`) %>%
  transmute(tCO2e = `ktCO2e_per_year` * 1000) %>%
  as.numeric()

painkillers_total = dat_tab1 %>%
  summarise(tCO2e = sum(`ktCO2e_per_year`) * 1000) %>%
  as.numeric()

knitr::kable(dat_tab1, caption = "Specific and total annual GHG footprints of four APIs in painkillers consumed in Austria 2014", 
             escape = F, 
             booktabs = TRUE,
             col.names = c("API name", "gCO2e/gAPI", "ktCO2e/year"))
```

<table>

<caption>

Specific and total annual GHG footprints of four APIs in painkillers
consumed in Austria 2014

</caption>

<thead>

<tr>

<th style="text-align:left;">

API name

</th>

<th style="text-align:right;">

gCO2e/gAPI

</th>

<th style="text-align:right;">

ktCO2e/year

</th>

</tr>

</thead>

<tbody>

<tr>

<td style="text-align:left;">

Paracetamol

</td>

<td style="text-align:right;">

7.8

</td>

<td style="text-align:right;">

0.40

</td>

</tr>

<tr>

<td style="text-align:left;">

Acetylsalicylic acid

</td>

<td style="text-align:right;">

4.9

</td>

<td style="text-align:right;">

0.16

</td>

</tr>

<tr>

<td style="text-align:left;">

Ibuprofen

</td>

<td style="text-align:right;">

3.1

</td>

<td style="text-align:right;">

0.10

</td>

</tr>

<tr>

<td style="text-align:left;">

Naproxen

</td>

<td style="text-align:right;">

2.3

</td>

<td style="text-align:right;">

0.03

</td>

</tr>

</tbody>

</table>

In the following, we present examples of the GHG emissions caused by the
production and use of four painkillers, antibiotics, anesthetic gases,
pressurized metered dose inhalers (pMDI), and two types of medical
gloves. With this selection we cover some of the most widely used
pharmaceuticals and medical products in the Austrian health care system.

``` r
#options(knitr.table.format = "latex")

dat_tab2 = read_excel(file_results, sheet = "Tab2_data")

antibiotics_intensity = dat_tab2 %>%
  filter(`API.Name` == 'Antibiotics') %>%
  select(`gCO2e_per_gAPI`) %>%
  as.numeric()

antibiotics_total = dat_tab2 %>%
  filter(`API.Name` == 'Antibiotics') %>%
  select(`ktCO2e_per_year`) %>%
  transmute(tCO2e = `ktCO2e_per_year` * 1000) %>%
  as.numeric()

knitr::kable(dat_tab2, caption = "Specific and total annual GHG footprints of the API in antibiotics and Amoxicillin consumed in Austria in 2014", 
             escape = F, 
             booktabs = TRUE,
             col.names = c("API name", "gCO2e/gAPI", "ktCO2e/year")) %>%
  footnote(general = "The estimated carbon intensity of antibiotics was applied to Amoxicillin")
```

<table>

<caption>

Specific and total annual GHG footprints of the API in antibiotics and
Amoxicillin consumed in Austria in 2014

</caption>

<thead>

<tr>

<th style="text-align:left;">

API name

</th>

<th style="text-align:right;">

gCO2e/gAPI

</th>

<th style="text-align:right;">

ktCO2e/year

</th>

</tr>

</thead>

<tbody>

<tr>

<td style="text-align:left;">

Antibiotics

</td>

<td style="text-align:right;">

14.3

</td>

<td style="text-align:right;">

1.0

</td>

</tr>

<tr>

<td style="text-align:left;">

Amoxicillin

</td>

<td style="text-align:right;">

14.3

</td>

<td style="text-align:right;">

0.3

</td>

</tr>

</tbody>

<tfoot>

<tr>

<td style="padding: 0; border: 0;" colspan="100%">

<span style="font-style: italic;">Note: </span>

</td>

</tr>

<tr>

<td style="padding: 0; border: 0;" colspan="100%">

<sup></sup> The estimated carbon intensity of antibiotics was applied to
Amoxicillin

</td>

</tr>

</tfoot>

</table>

### Painkillers and antibiotics

The specific GHG footprints of the four painkillers range between `2.3`
(naproxen) and `7.8` (paracetamol) gCO2e/gAPI (active pharmaceutical
ingredient) (table 1). The GHG emissions (direct energy use in
production) of antibiotics are estimated at `14.3` gCO2/gAPI (table 2).
The total annual GHG footprint of Paracetamol in 2014 was `400` tCO2e
and of the APIs in antibiotics `1000` tCO2e. Even though the emission
intensities (gCO2/gAPI) of these four painkillers are comparatively low,
due to their broad use they account for `690` tCO2e of total emissions.
Overall, the range of GHG intensities of pharmaceutical APIs is
extremely large. Depending on molecule complexity and the number of
synthesis steps in manufacturing (Parvatker et al. 2019), some APIs can
have GHG intensities between hundreds (e.g. morphine (McAlister et al.
2016)) or several thousands of gCO2e/gAPI (Parvatker et al. 2019). For
an unnamed but typical API, Werner et al. (2010) estimated the GHG
footprint at 67.7 gCO2e/gAPI. Overprescription, polypharmacy and
potentially inadequate medication of antibiotics and painkillers pose
serious global health problems (Ghafur 2015). Intoxications due to
overuse of paracetamol are common in many countries worldwide and are
the leading cause of acute liver failure in Europe and the US (Bateman
et al. 2014). Considered together with their climate effects they are a
“hot spot” for intervention and further investigation (SDU and ERM
2014).

Overprescription of pharmaceuticals in health care systems more
generally has previously been criticized in many countries including
Austria, mainly on the grounds of rising costs (Schweitzer and Lu 2018)
and adverse health effects of overconsumption, particularly in elderly
patients (Cassel 2012; Gogol and Siebenhofer 2016; Mann et al. 2014;
Hajjar, Cafiero, and Hanlon 2007). In light of the substantial GHG
emissions associated with pharmaceuticals, eliminating overprescription
of pharmaceuticals would also help to reduce GHG emissions from health
care. Yet, strategies to curtail overconsumption of pharmaceuticals have
largely failed, because of fragmented responsibilities, lacking
integration of care, poor interface and quality management, and the
vested interests of the pharmaceutical industries (Deangelis 2016). In
Austria, expenditure on pharmaceuticals continues to rise in the
inpatient and outpatient sector. In public hospitals, costs for
pharmaceuticals increased with an average growth rate of 4.7% between
2010 and 2017 while total final costs grew with an average rate of 3.3%.
Also in the outpatient sector, higher growth rates for pharmaceuticals
(3.7%) as compared to total growth of the sector (3.4%) could be
observed (OECD 2018).

``` r
#options(knitr.table.format = "latex")

dat_tab3 = read_excel(file_results, sheet = "Tab3_data")


nox_total_2015 = dat_tab3 %>%
  filter(`API.Name` == 'Nitrous oxide (N2O)', Year==2015) %>%
  pull(value)

anesthetic_total_2010 = dat_tab3 %>%
  filter(`API.Name` == 'Sum anesthetic gases', Year==2010) %>%
  pull(value)

anesthetic_total_2015 = dat_tab3 %>%
  filter(`API.Name` == 'Sum anesthetic gases', Year==2015) %>%
  pull(value)

pmdi_total_2015 = dat_tab3 %>%
  filter(`API.Name` == 'pMDI', Year==2015) %>%
  pull(value)

dat_tab3t = dat_tab3 %>%
  pivot_wider(names_from = Year, values_from = value) %>%
  select(-unit)
  

knitr::kable(dat_tab3t, caption = "GHG emissions from the use of anesthetic gases and pMDI in Austria: 2005, 2010 and 2015", 
             escape = F, 
             booktabs = TRUE,
             col.names = c("API name", "2005", "2010", "2015")) %>%
  add_header_above(c(" ", "ktCO2e/year" = 3))
```

<table>

<caption>

GHG emissions from the use of anesthetic gases and pMDI in Austria:
2005, 2010 and 2015

</caption>

<thead>

<tr>

<th style="border-bottom:hidden" colspan="1">

</th>

<th style="border-bottom:hidden; padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="3">

<div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">

ktCO2e/year

</div>

</th>

</tr>

<tr>

<th style="text-align:left;">

API name

</th>

<th style="text-align:left;">

2005

</th>

<th style="text-align:left;">

2010

</th>

<th style="text-align:left;">

2015

</th>

</tr>

</thead>

<tbody>

<tr>

<td style="text-align:left;">

Desflurane

</td>

<td style="text-align:left;">

  - 
    
    </td>
    
    <td style="text-align:left;">
    
    1.6
    
    </td>
    
    <td style="text-align:left;">
    
    0.6
    
    </td>
    
    </tr>
    
    <tr>
    
    <td style="text-align:left;">
    
    Sevoflurane
    
    </td>
    
    <td style="text-align:left;">
    
      - 
        
        </td>
        
        <td style="text-align:left;">
        
        1.2
        
        </td>
        
        <td style="text-align:left;">
        
        1.3
        
        </td>
        
        </tr>
        
        <tr>
        
        <td style="text-align:left;">
        
        Nitrous oxide (N2O)
        
        </td>
        
        <td style="text-align:left;">
        
          - 
            
            </td>
            
            <td style="text-align:left;">
            
            28.2
            
            </td>
            
            <td style="text-align:left;">
            
            19.5
            
            </td>
            
            </tr>
            
            <tr>
            
            <td style="text-align:left;">
            
            Sum anesthetic gases
            
            </td>
            
            <td style="text-align:left;">
            
              - 
                
                </td>
                
                <td style="text-align:left;">
                
                31
                
                </td>
                
                <td style="text-align:left;">
                
                21.4
                
                </td>
                
                </tr>
                
                <tr>
                
                <td style="text-align:left;">
                
                pMDI
                
                </td>
                
                <td style="text-align:left;">
                
                13
                
                </td>
                
                <td style="text-align:left;">
                
                21.6
                
                </td>
                
                <td style="text-align:left;">
                
                25.9
                
                </td>
                
                </tr>
                
                </tbody>
                
                </table>

### Anesthetic gases and pressurized metered dose inhalers

Anesthetic gases are used to maintain and induce anesthesia due to their
hypnotic, analgesic and/or muscle relaxant effects. They include the
hydrofluorocarbons sevoflurane and desflurane, the chlorofluorocarbon
isoflurane, and nitrous oxide (commonly known as laughing gas).
Anesthetic gases are potent GHGs, their global warming potential is 2-3
orders of magnitude larger than that of CO2 (Andersen et al. 2012;
Vollmer et al. 2015). The release of anesthetic gases to the atmosphere
occurs during application. In Austrian hospitals the use of anesthetic
gases led to more than `21.4` ktCO2e of emissions in 2015, mainly from
the use of nitrous oxide (`19.5` ktCO2e). Due to shorter duration of
anesthesia during operations and more local anesthetics, the GHG
emissions from anesthetic gases in Austrian hospitals have declined
since 2010 (`31` ktCO2e). This declining trend in the overall use of
anesthetic gases could also be observed in other countries (UBA 2019, p
286). At the same time, the global use of desflurane, the most damaging
agent, is increasing (Vollmer et al. 2015). Xenon has been discussed as
an alternative to GHG intensive anesthetic gases, but due to higher
costs it is used only rarely at present (Charlesworth and Swinton 2017).
Other alternative intravenous drugs exist with GHG emissions several
orders of magnitude lower than that of inhaled anesthetic drugs
(Parvatker et al. 2019; Sherman et al. 2012).

GHGs released through the use of pressurized metered dose inhalers
(pMDI) have doubled since 2005 and they are responsible for `25.9`
ktCO2e in 2015. Pressurized metered dose inhalers are frequently used
for inhalation therapy of respiratory diseases. Primarily these are the
lifestyle-related chronic obstructive pulmonary disease (COPD) and
asthma, which is associated with a climate-induced increase in pollen
allergies (Beggs and Bambrick 2005; Hamaoui-Laguel et al. 2015). The
increase in GHG emissions might therefore result from the increased
disease burden in Austria. Compared to other European countries,
however, the use of pMDI for respiratory diseases in Austria is low
(Lavorini et al. 2011). Alternatives exist in the form of dry-powder
inhalers (DPIs) and liquids for nebulization (Hillman, Mortimer, and
Hopkinson 2013). In order to curb pDMI usage, NHS England recommends to
reduce unnecessary prescription of inhalers, to shift to lower carbon
options where clinically appropriate, and to avoid misuse through
inhaler use training (Wilkinson et al. 2019; SDU 2019).

### Medical gloves

Medical gloves are among the most cost-intensive medical non-durable
consumer goods used in Austrian hospitals (source: HA) and elsewhere
(SDU and ERM 2017). According to our extrapolation (from HA), Austrian
hospitals used 450 million single medical gloves worth nearly 12 million
Euros annualy (average over 2008-2015). Medical gloves used in Austrian
hospitals are mostly latex or nitrile gloves (97%) with GHG intensities
3.3 gCO2e/g and 11.7 gCO2e/g, respectively. Total GHG emissions
attributable to their production amounted to nearly 11 ktCO2e per year
(average over 2008-2015). There is evidence that using cheaper, low
quality medical gloves, as purchased in hospitals to save costs, in fact
lead to higher total costs and higher upstream emissions, because in
actual practice often two or three gloves are used on top of each other
to compensate for their low quality (HCWH 2018).

## Induced travel

``` r
dat_fig4 = read_excel(file_results, sheet = "Fig4_data") 

dat_fig4_sum = dat_fig4 %>%
  group_by(Year) %>%
  summarise(ktCO2 = sum(ktCO2))
  
trend_induced = as.integer(round(dat_fig4_sum[dat_fig4_sum$Year == 2015, "ktCO2"] / 
  dat_fig4_sum[dat_fig4_sum$Year == 2005, "ktCO2"]*100-100))

dat_fig4_share = dat_fig4 %>%
  group_by(Year) %>%
  mutate(percent = ktCO2/sum(ktCO2)*100)

mean_share_ind_amb_pat = dat_fig4_share %>%
  filter(Provider == "Ambulatory" & Category == "Patients") %>%
  ungroup() %>%
  summarise(percent = round(mean(percent)))

mean_share_ind_hosp = dat_fig4_share %>%
  filter(Provider == "Hospitals") %>%
  group_by(Year, Provider) %>%
  summarise(percent = sum(percent)) %>%
  ungroup() %>%
  summarise(percent = round(mean(percent)))

induced_increase = round(dat_fig4_sum[dat_fig4_sum$Year==2015, "ktCO2"]-dat_fig4_sum[dat_fig4_sum$Year==2005, "ktCO2"])
```

Patients, staff and visitors need transportation to access health care
providers, their work place, or their sick relatives. These trips cause
CO2 emissions, dependent on the mode of transportation, and the
frequency and lengths of the trips. Induced travel emissions are not
part of the health care system as defined by SHA, and are therefore not
included in the MRIO-based health carbon footprint. However, knowing
their magnitude can reveal important information about emission
reduction potentials through better transportation logistics and better
spatial distribution of health care providers. We calculated CO2
emissions from patient, staff and visitor trips to and from hospitals
and ambulatory health care for the years 2005, 2010 and 2015 (Figure
@ref(fig:fig4)).

Three findings stand out: Induced travel causes roughly the same amount
of CO2 emissions as the health care sector’s direct energy consumption.
Unlike the latter, CO2 emissions from induced travel increased by `15`%
from 2005 to 2015. Patient trips to ambulatory health care providers
accounted for most of the increase and is also the largest single
category with an average share of `59`%, compared to an average of `35`%
for all induced travel to and from hospitals.

The observed trend of patient trips to ambulatory health care providers
has three underlying reasons. First, overall higher demand has increased
the total number of trips (particularly of patients). Secondly, the
length of trips has increased (particularly in rural areas), likely
reflecting a trend in Austrian health care planning to centralize
specific health services for efficiency and quality reasons (Bachner,
Bobek, Habimana, et al. 2018). Finally, the energy efficiency of the
Austrian private car fleet has decreased between 2010 and 2015 which is
likely explained by increases in average car size. Together these
factors resulted in an additional `109` ktCO2 emissions from induced
travel between 2005 and 2015.

The growing difficulties to provide sufficient ambulatory health care
capacities in thinly populated and remote rural areas is a well-known
problem of the Austrian health care system, and certainly contributed to
these developments. Demographic changes like ageing in rural areas
further worsen the situation and add a social dimension to the rising
environmental cost of accessing ambulatory health care provision. The
observed development of trips to ambulatory health care providers
certainly needs to be investigated in more detail. From our results we
conclude that health policies which influence the location of ambulatory
health care should include environmental and social costs in their goal
definitions, especially the accessibility via low carbon transport
modes.

``` r
knitr::include_graphics(normalizePath(paste0(here(),"/figures/","figure4.pdf")))
```

<div class="figure" style="text-align: center">

<img src="/home/pichler/gits/carbon-emission-trends-and-sustainability-options-in-austrian-health-care/figures/figure4.pdf" alt="Health care sector induced travel by hospitals and ambulatory health care: CO2 emissions 2005, 2010 and 2015" width="85%" />

<p class="caption">

Health care sector induced travel by hospitals and ambulatory health
care: CO2 emissions 2005, 2010 and 2015

</p>

</div>

# Conclusions for sustainable health care

The health care sector cannot rely exclusively on external factors like
the decarbonization of energy systems in its contribution to climate
change mitigation. For one thing, these efforts will become increasingly
difficult once the cheapest and less controversial options to improve
the carbon efficiency of the energy sector and along the health care
sector’s supply chains are exhausted and more stringent policies are
needed to achieve further emission reductions (Le Quéré et al. 2019).
More importantly, ageing populations, a growing number of chronic-non
communicable diseases, technological progress (EC 2018), as well as
increasing impacts of climate change on human health are predicted to
increase the future demand of health services (Watts et al. 2018; WHO
2018; APCC 2018; Salas and Jha 2019). This may well offset any gains in
GHG efficiency made outside the health care sector and makes it
essential to step up climate change mitigation efforts within the health
care sector.

The health care sector has long been excluded from climate change
mitigation, mainly because health care is indispensable for human
wellbeing. However, as our results and an increasing body of literature
from environmental sciences and public health shows, many untapped
possibilities for reducing the carbon footprint of health care exist.
Many of these measures would not undermine health service provision and
in some cases would even be beneficial for population health.

We see six major areas of intervention that are applicable to most
health care systems in industrialized countries. We discuss the areas in
ascending order of the severity of the intervention required: 1) direct
energy use, 2) product alternatives, 3) avoidance of inefficiencies in
the health care system (including the rational use of pharmaceuticals),
4) changing medical treatments 5) changes in national health care
provision planning, and 6) the transformation of the health care system
towards promoting human and planetary health.

The first area addresses “non-health care specific” emission reductions
ranging from more efficient energy use to a switch to low carbon
alternatives. Known key intervention points are the thermal insulation
of buildings, the implementation of carbon efficient heating and cooling
systems, the use of energy efficient cars and devices, and energy
savings related to lighting or the handling of energy intensive
appliances (HCWH 2018, 2016; SDU 2014; Tomson 2015). Where available a
switch to a renewable power supply would reduce the upstream emissions
related to electricity consumption.

The large contribution of pharmaceuticals and medical products to the
health carbon footprint suggests that greening strategies in health care
should be extended to the medical area (APCC 2018; SDU and ERM 2014,
2017; WBG 2017; Weisz et al. 2011; WHO 2018). Thus, the second area
comprises low-carbon pharmaceuticals and other medical products.
Emissions could be reduced by using functionally equivalent but less
carbon intensive drugs and medical devices (Belboom et al. 2011;
Hillman, Mortimer, and Hopkinson 2013; Parvatker et al. 2019).

Product hot spots include the use of greenhouse gas containing
anesthetic gases (Charlesworth and Swinton 2017) and pressurized metered
dose inhalers (SDU 2019). Other product hot spots are non-durables that
are consumed in high quantities, for example medical gloves (SDU and ERM
2017).

The third area touches core inefficiencies of health care systems like
unnecessary or even harmful treatments, which are a well-documented
problem in Austria and many other health care systems (OECD 2017b; Saini
et al. 2017). Reduction strategies should first of all target wasteful
practices e.g., reducing the amount of pharmaceuticals purchased but not
consumed (Vogler et al. 2014; Nansai et al. 2020). Further,
overprescription, which in the case of antibiotics is harmful not only
for individual but for global health (Ghafur 2015; Tamma and Cosgrove
2014) should be avoided. The occurrence of unnecessary invasive or
multiple examinations, ineffective and inappropriate care or duplication
of services is surprisingly high in many health care systems, including
the Austrian (Beitel et al. 2016; OECD 2017a). Intervention options to
reduce such inefficiencies are in line with long-standing reform
strategies in Austria’s health policy (Bachner, Bobek, Lepuschütz, et
al. 2018).

More innovative approaches are summarized in the fourth area and include
low-carbon medical procedures and sustainable provision planning of
specific health services. Such services include alternative low-carbon
treatments, the implementation of new health care settings in hospitals
or telemedicine services for the chronically ill (Connor, Lillywhite,
and Cooke 2010; Holmner et al. 2014; Weisz et al. 2011).

The fifth area comprises changes in national health care provision
planning. A stronger focus on shifting unnecessary acute care in
hospitals to low-carbon forms of health care provision would help to
reduce demand on the hospital sector, known as the most costly and
carbon-intensive health care provider. The increasing CO2 emissions from
private travel induced by health care providers indicate that wider
effects should be considered in policies of national infrastructural
health care provision planning, e.g. the availability of low carbon
modes of transport. A particular challenge in this regard is providing
access to ambulatory health care in thinly populated and remote rural
areas. In this context, telehealth might be a useful option (Dorsey and
Topol 2016).

Finally, the most radical approach to sustainable health care would be
to extend the mission of health care systems towards promoting human and
planetary health (APCC 2018; Borowy and Aillon 2017; Walpole et al.
2019; Tamma and Cosgrove 2014; Weisz 2016). This would mean a systemic
transformation to strengthen preventive care and health promotion to
improve public health, lower the health service demand and consequently
the carbon footprint of health care regardless of any efforts outside
the health care sector to decarbonize the energy system. The fact that
Austria’s expenditure on preventive care has always been marginal, 0.8%
of total health expenditure in 2015 (OECD 2018), indicates that there is
a significant improvement potential.

Overall, minimizing deficiencies and implementing options for climate
and health across those areas would have multiple benefits: avoiding
adverse health effects, improving quality of life, saving costs and
reducing carbon emissions. Comprehensive quantifications of those
benefits are currently not available, but existing evidence suggests
that they are substantial (OECD 2017a; Watts et al. 2017, 2019; WHO
2018). The development of a specific climate strategy for the health
care sector, as implemented by the English National Health Service (SDU
2014) and proposed by (APCC 2018; HCWH 2016; WBG 2017), would be an
important step for realizing sustainable health care.

# Acknowledgments

The authors acknowledge funding by the Austrian Climate and Energy Fund
carried out within the framework of the ‘ACRP’ Program (Project Number
KR16AC0K13225, HealthFootprint). We thank, Andreas Windsperger and
Bernhard Windsperger for providing estimates of LCA-based emission
factors, Lena Lepuschütz for sharing her insights of the Austrian health
care sector, Paul Brockway and Dominik Wiedenhofer for their comments on
an earlier version of the manuscript, and Chiemi Miura for graphic
design.

# References

``` r
sessionInfo()
```

<div id="refs" class="references">

<div id="ref-andersenAssessingImpactGlobal2012">

Andersen, Mads P. Sulbaek, Ole J. Nielsen, Timothy J. Wallington, Boris
Karpichev, and Stanley P. Sander. 2012. “Assessing the Impact on Global
Climate from General Anesthetic Gases:” *Anesthesia & Analgesia* 114
(5): 1081–5. <https://doi.org/10.1213/ANE.0b013e31824d6150>.

</div>

<div id="ref-apccOsterreichischerSpecialReport2018">

APCC, ed. 2018. *Österreichischer Special Report Gesundheit, Demographie
Und Klimawandel (ASR18).* Wien: Austrian Panel on Climate Change (APCC),
Österreichische Akademie der Wissenschaften.

</div>

<div id="ref-bachnerAustriaHealthSystem2018">

Bachner, Florian, Julia Bobek, K. Habimana, J. Ladurner, Lena
Lepuschutz, H. Ostermann, L. Rainer, et al. 2018. “Austria. Health
System Review.” Health Systems in Transition 20 (3). Health Systems in
Transition.

</div>

<div id="ref-bachnerMonitoringberichtZielsteuerungGesundheitMonitoring2018">

Bachner, Florian, Julia Bobek, Lena Lepuschütz, Lukas Rainer, and Martin
Zuba. 2018. “Monitoringbericht Zielsteuerung-Gesundheit. Monitoring Nach
Vereinbarung Gemäß Art 15a B-VG Zielsteuerung-Gesundheit Und
Zielsteuerungsvertrag.” Wien: Gesundheit Österreich.

</div>

<div id="ref-basgUbersichtUberZugelassene2019">

BASG. 2019. “Übersicht über Zugelassene/Registrierte Arzneimittel Und
Antragskategorien (Stand: 01.01.2019).” Bundesamt für Sicherung im
Gesundheitswesen. Arzneimittel in Österreich. 2019.
<https://www.basg.gv.at/news-center/statistiken/arzneimittel-in-oesterreich/>.

</div>

<div id="ref-batemanReductionAdverseEffects2014">

Bateman, D Nicholas, James W Dear, H K Ruben Thanacoody, Simon H L
Thomas, Michael Eddleston, Euan A Sandilands, Judy Coyle, et al. 2014.
“Reduction of Adverse Effects from Intravenous Acetylcysteine
Treatment for Paracetamol Poisoning: A Randomised Controlled Trial.”
*The Lancet* 383 (9918): 697–704.
<https://doi.org/10.1016/S0140-6736(13)62062-0>.

</div>

<div id="ref-beggsGlobalRiseAsthma2005">

Beggs, Paul John, and Hilary Jane Bambrick. 2005. “Is the Global Rise of
Asthma an Early Impact of Anthropogenic Climate Change?” *Environmental
Health Perspectives* 113 (8): 915–19.
<https://doi.org/10.1289/ehp.7724>.

</div>

<div id="ref-beitelPerformancemessungImOsterreichischen2016">

Beitel, Christoph, Anna Labek, Aline Dragosits, Andreas Goltz, Nina
Pfeffer, and Martin Scheuringer. 2016. “Performancemessung Im
österreichischen Gesundheitswesen - Schwerpunkt: Outcomes.
SV-Analysenbericht. Machbarkeitsstudie Zur Analyse von Wirkketten Für
PYLL (Potenziell Verlorene Lebensjahre) Und PIM (Potenzill Inadäquate
Medikaiton von Älteren).” Wien, Linz: Oberösterreichische
Gebietskrankenkasse, Hauptverband der österreichischen
Sozialversicherungsträger.

</div>

<div id="ref-belboomLifeCycleAssessment2011">

Belboom, Sandra, Robert Renzoni, Benoît Verjans, Angélique Léonard, and
Albert Germain. 2011. “A Life Cycle Assessment of Injectable Drug
Primary Packaging: Comparing the Traditional Process in Glass Vials with
the Closed Vial Technology (Polymer Vials).” *The International Journal
of Life Cycle Assessment* 16 (2): 159–67.
<https://doi.org/10.1007/s11367-011-0248-z>.

</div>

<div id="ref-belkhirCarbonFootprintGlobal2019">

Belkhir, Lotfi, and Ahmed Elmeligi. 2019. “Carbon Footprint of the
Global Pharmaceutical Industry and Relative Impact of Its Major
Players.” *Journal of Cleaner Production* 214 (March): 185–94.
<https://doi.org/10.1016/j.jclepro.2018.11.204>.

</div>

<div id="ref-bmasgkAustrianHealthSystem2018">

BMASGK. 2018. “Austrian Health System Statistics. Dokumentations- Und
Informationssystem Für Analysen Im Gesundheitswesen. Non-Public Data
Base (Accessed 7.6.2019).” [non-public data base
(accessed 7.6.2019)](non-public%20data%20base%20\(accessed%207.6.2019\)).

</div>

<div id="ref-bmasgkBettenKrankenanstaltenTatsachlich2019">

———. 2019. “Betten in Krankenanstalten. Tatsächlich Aufgestellte
Betten.” Bundesministerium für Arbeit, Soziales, Gesundheit und
Konsumentenschutz. Krankenanstalten in Zahlen. 2019.
<http://www.kaz.bmgf.gv.at/ressourcen-inanspruchnahme/betten.html>.

</div>

<div id="ref-borowySustainableHealthDegrowth2017">

Borowy, Iris, and Jean-Louis Aillon. 2017. “Sustainable Health and
Degrowth: Health, Health Care and Society Beyond the Growth Paradigm.”
*Social Theory & Health*, April.
<https://doi.org/10.1057/s41285-017-0032-7>.

</div>

<div id="ref-brownEstimatingLifeCycle2012">

Brown, Lawrence H., Petra G. Buettner, Deon V. Canyon, J. Mac Crawford,
and Jenni Judd. 2012. “Estimating the Life Cycle Greenhouse Gas
Emissions of Australian Ambulance Services.” *Journal of Cleaner
Production* 37 (December): 135–41.
<https://doi.org/10.1016/j.jclepro.2012.06.020>.

</div>

<div id="ref-campionLifeCycleAssessment2012">

Campion, Nicole, Cassandra L. Thiel, Justin DeBlois, Noe C. Woods, Amy
E. Landis, and Melissa M. Bilec. 2012. “Life Cycle Assessment
Perspectives on Delivering an Infant in the US.” *Science of the Total
Environment* 425 (May): 191–98.
<https://doi.org/10.1016/j.scitotenv.2012.03.006>.

</div>

<div id="ref-casselChoosingWiselyHelping2012">

Cassel, Christine K. 2012. “Choosing Wisely: Helping Physicians and
Patients Make Smart Decisions About Their Care.” *JAMA* 307 (17): 1801.
<https://doi.org/10.1001/jama.2012.476>.

</div>

<div id="ref-charlesworthAnaestheticGasesClimate2017">

Charlesworth, Michael, and Frank Swinton. 2017. “Anaesthetic Gases,
Climate Change, and Sustainable Practice.” *The Lancet Planetary Health*
1 (6): e216–e217.
<https://doi.org/https://doi.org/10.1016/S2542-5196(17)30040-2>.

</div>

<div id="ref-chungEstimateCarbonFootprint2009">

Chung, Jeanette W., and David O. Meltzer. 2009. “Estimate of the Carbon
Footprint of the US Health Care Sector.” *JAMA* 302 (18): 1970–2.
<https://doi.org/10.1001/jama.2009.1610>.

</div>

<div id="ref-cimprichPotentialIndustrialEcology2019">

Cimprich, Alexander, Jair Santillán‐Saldivar, Cassandra L. Thiel, Guido
Sonnemann, and Steven B. Young. 2019. “Potential for Industrial Ecology
to Support Healthcare Sustainability: Scoping Review of a Fragmented
Literature and Conceptual Framework for Future Research.” *Journal of
Industrial Ecology*, May. <https://doi.org/10.1111/jiec.12921>.

</div>

<div id="ref-connorCarbonFootprintRenal2010">

Connor, A., R. Lillywhite, and M. W. Cooke. 2010. “The Carbon Footprint
of a Renal Service in the United Kingdom.” *QJM* 103 (12): 965–75.
<https://doi.org/10.1093/qjmed/hcq150>.

</div>

<div id="ref-deangelisBigPharmaProfits2016">

Deangelis, Catherine D. 2016. “Big Pharma Profits and the Public Loses.”
*The Milbank Quarterly* 94 (1): 30–33.
<https://doi.org/10.1111/1468-0009.12171>.

</div>

<div id="ref-desoeteChallengesRecommendationsEnvironmental2017">

De Soete, Wouter, Concepción Jiménez-González, Phil Dahlin, and Jo
Dewulf. 2017. “Challenges and Recommendations for Environmental
Sustainability Assessments of Pharmaceutical Products in the Healthcare
Sector.” *Green Chemistry* 19 (15): 3493–3509.
<https://doi.org/10.1039/C7GC00833C>.

</div>

<div id="ref-dorseyStateTelehealth2016">

Dorsey, E. Ray, and Eric J. Topol. 2016. “State of Telehealth.” Edited
by Edward W. Campion. *New England Journal of Medicine* 375 (2): 154–61.
<https://doi.org/10.1056/NEJMra1601705>.

</div>

<div id="ref-ec2018AgeingReport2018">

EC. 2018. *The 2018 Ageing Report. Economic a& Budgetary Projections for
the 28 EU Member States (2016-2070).* European Economy, 079 | May 2018.
Luxembourg: Publications Office of the European Union.

</div>

<div id="ref-eckelmanComparativeLifeCycle2012">

Eckelman, Matthew J., Margo Mosher, Andres Gonzalez, and Jodi Sherman.
2012. “Comparative Life Cycle Assessment of Disposable and Reusable
Laryngeal Mask Airways:” *Anesthesia & Analgesia* 114 (5): 1067–72.
<https://doi.org/10.1213/ANE.0b013e31824f6959>.

</div>

<div id="ref-eckelmanEnvironmentalImpactsHealth2016">

Eckelman, Matthew J., and Jodi Sherman. 2016. “Environmental Impacts of
the U.S. Health Care System and Effects on Public Health.” *PLoS ONE* 11
(6): e0157014. <https://doaj.org>.

</div>

<div id="ref-eckelmanLifeCycleEnvironmental2018">

Eckelman, Matthew J., Jodi D. Sherman, and Andrea J. MacNeill. 2018.
“Life Cycle Environmental Emissions and Health Damages from the
Canadian Healthcare System: An Economic-Environmental-Epidemiological
Analysis.” *PLOS Medicine* 15 (7): e1002623.
<https://doi.org/10.1371/journal.pmed.1002623>.

</div>

<div id="ref-gaoAnalysisEnergyrelatedCO22019">

Gao, Ziyan, Yong Geng, Rui Wu, Wei Chen, Fei Wu, and Xu Tian. 2019.
“Analysis of Energy-Related CO2 Emissions in China’s Pharmaceutical
Industry and Its Driving Forces.” *Journal of Cleaner Production* 223
(June): 94–108. <https://doi.org/10.1016/j.jclepro.2019.03.092>.

</div>

<div id="ref-ghafurOverconsumptionAntibiotics2015">

Ghafur, Abdul. 2015. “Overconsumption of Antibiotics.” *The Lancet
Infectious Diseases* 15 (4): 377.
<https://doi.org/10.1016/S1473-3099(15)70081-2>.

</div>

<div id="ref-gogolChoosingWiselyGegen2016">

Gogol, Manfred, and Andrea Siebenhofer. 2016. “Choosing Wisely – Gegen
Überversorgung Im Gesundheitswesen – Aktivitäten Aus Deutschland Und
Österreich Am Beispiel Der Geriatrie.” *Wiener Medizinische
Wochenschrift* 166 (5): 155–60.
<https://doi.org/10.1007/s10354-015-0424-z>.

</div>

<div id="ref-hajjarPolypharmacyElderlyPatients2007">

Hajjar, Emily R., Angela C. Cafiero, and Joseph T. Hanlon. 2007.
“Polypharmacy in Elderly Patients.” *The American Journal of Geriatric
Pharmacotherapy* 5 (4): 345–51.
<https://doi.org/10.1016/j.amjopharm.2007.12.002>.

</div>

<div id="ref-hamaoui-laguelEffectsClimateChange2015">

Hamaoui-Laguel, Lynda, Robert Vautard, Li Liu, Fabien Solmon, Nicolas
Viovy, Dmitry Khvorostyanov, Franz Essl, et al. 2015. “Effects of
Climate Change and Seed Dispersal on Airborne Ragweed Pollen Loads in
Europe.” *Nature Climate Change* 5 (8): 766–71.
<https://doi.org/10.1038/nclimate2652>.

</div>

<div id="ref-hcwhReducingHealthCare2016">

HCWH. 2016. “Reducing Health Care´s Climate Footprint. Opportunities for
European Hospitals and Health Systems.” Brussels: HealthCare Without
Harm.

</div>

<div id="ref-hcwhReducingCarbonFootprint2018">

———. 2018. “Reducing the Carbon Footprint of Healthcare Through
Sustainable Procurement.” Brussels: Health Care Without Harm Europe.

</div>

<div id="ref-hcwhHealthCareClimate2019">

HCWH, and ARUP. 2019. “Health Care´s Climate Footprint. How the Health
Sector Contributes to the Global Crises and Opportunities for Action.”
1. Climate-Smart Health Care Series, Green Paper No. One. HealthCare
Without Harm.

</div>

<div id="ref-hillmanInhaledDrugsGlobal2013">

Hillman, T., F. Mortimer, and N. S. Hopkinson. 2013. “Inhaled Drugs and
Global Warming: Time to Shift to Dry Powder Inhalers.” *BMJ* 346 (may28
4): f3359–f3359. <https://doi.org/10.1136/bmj.f3359>.

</div>

<div id="ref-holmnerCarbonFootprintTelemedicine2014">

Holmner, Åsa, Kristie L. Ebi, Lutfan Lazuardi, and Maria Nilsson. 2014.
“Carbon Footprint of Telemedicine Solutions - Unexplored Opportunity
for Reducing Carbon Emissions in the Health Sector.” *PLoS One* 9 (9):
e105040. <https://doi.org/10.1371/journal.pone.0105040>.

</div>

<div id="ref-ipccGlobalWarmingIPCC2018">

IPCC. 2018. *Global Warming of 1.5°C. An IPCC Special Report on the
Impacts of Global Warming of 1.5°C Above Pre-Industrial Levels and
Related Global Greenhouse Gas Emission Pathways, in the Context of
Strengthening the Global Response to the Threat of Climate Change,
Sustainable Development, and Efforts to Eradicate Poverty*. Geneva,
Switzerland: World Meteorological Organization,
<http://www.ipcc.ch/report/sr15/>.

</div>

<div id="ref-lavoriniRetailSalesInhalation2011">

Lavorini, F., C.J. Corrigan, P.J. Barnes, P.R.N. Dekhuijzen, M.L. Levy,
S. Pedersen, N. Roche, W. Vincken, and G.K. Crompton. 2011. “Retail
Sales of Inhalation Devices in European Countries: So Much for a Global
Policy.” *Respiratory Medicine* 105 (7): 1099–1103.
<https://doi.org/10.1016/j.rmed.2011.03.012>.

</div>

<div id="ref-lenzenErrorsConventionalInputOutputbased2000">

Lenzen, Manfred. 2000. “Errors in Conventional and Input-Output-Based
Life-Cycle Inventories.” *Journal of Industrial Ecology* 4 (4): 127–48.
<https://doi.org/10.1162/10881980052541981>.

</div>

<div id="ref-lenzenMappingStructureWorld2012">

Lenzen, Manfred, Keiichiro Kanemoto, Daniel Moran, and Arne Geschke.
2012. “Mapping the Structure of the World Economy.” *Environmental
Science & Technology* 46 (15): 8374–81.
<https://doi.org/10.1021/es300171x>.

</div>

<div id="ref-lenzenBuildingEoraGlobal2013">

Lenzen, Manfred, Daniel Moran, Keiichiro Kanemoto, and Arne Geschke.
2013. “Building Eora: A Global Multi-Region Input–Output Database at
High Country and Sector Resolution.” *Economic Systems Research* 25 (1):
20–49. <https://doi.org/10.1080/09535314.2013.769938>.

</div>

<div id="ref-lequereDriversDecliningCO22019">

Le Quéré, Corinne, Jan Ivar Korsbakken, Charlie Wilson, Jale Tosun,
Robbie Andrew, Robert J. Andres, Josep G. Canadell, Andrew Jordan, Glen
P. Peters, and Detlef P. van Vuuren. 2019. “Drivers of Declining CO2
Emissions in 18 Developed Economies.” *Nature Climate Change* 9 (3):
213–17. <https://doi.org/10.1038/s41558-019-0419-7>.

</div>

<div id="ref-macneillImpactSurgeryGlobal2017">

MacNeill, Andrea J, Robert Lillywhite, and Carl J Brown. 2017. “The
Impact of Surgery on Global Climate: A Carbon Footprinting Study of
Operating Theatres in Three Health Systems.” *The Lancet Planetary
Health* 1 (9): e381–e388.
<https://doi.org/10.1016/S2542-5196(17)30162-6>.

</div>

<div id="ref-majeau-bettezEvaluationProcessInput2011">

Majeau-Bettez, Guillaume, Anders Hammer Strømman, and Edgar G. Hertwich.
2011. “Evaluation of Process- and Input–Output-Based Life Cycle
Inventory Data with Regard to Truncation and Aggregation Issues.”
*Environmental Science & Technology* 45 (23): 10170–7.
<https://doi.org/10.1021/es201308x>.

</div>

<div id="ref-malikCarbonFootprintAustralian2018">

Malik, Arunima, Manfred Lenzen, Scott McAlister, and Forbes McGain.
2018. “The Carbon Footprint of Australian Health Care.” *The Lancet
Planetary Health* 2 (1): e27–e35.
<https://doi.org/10.1016/S2542-5196(17)30180-8>.

</div>

<div id="ref-mannPotentiallyInappropriateMedication2014">

Mann, E., B. Haastert, T. Frühwald, R. Sauermann, M. Hinteregger, D.
Hölzl, S. Keuerleber, M. Scheuringer, and G. Meyer. 2014. “Potentially
Inappropriate Medication in Older Persons in Austria: A Nationwide
Prevalence Study.” *European Geriatric Medicine* 5 (6): 399–405.
<https://doi.org/10.1016/j.eurger.2014.06.035>.

</div>

<div id="ref-mcalisterEnvironmentalFootprintMorphine2016">

McAlister, Scott, Yanjun Ou, Elise Neff, Karen Hapgood, David Story,
Philip Mealey, and Forbes McGain. 2016. “The Environmental Footprint of
Morphine: A Life Cycle Assessment from Opium Poppy Farming to the
Packaged Drug.” *BMJ Open* 6 (10): e013302.
<https://doi.org/10.1136/bmjopen-2016-013302>.

</div>

<div id="ref-mcgainFinancialEnvironmentalCosts2010">

McGain, F., S. McAlister, A. McGavin, and D. Story. 2010. “The Financial
and Environmental Costs of Reusable and Single-Use Plastic Anaesthetic
Drug Trays.” *Anaesthesia and Intensive Care* 38 (3): 538–44.

</div>

<div id="ref-mcgainEnvironmentalSustainabilityHospitals2014">

McGain, Forbes, and Chris Naylor. 2014. “Environmental Sustainability in
Hospitals – a Systematic Review and Research Agenda.” *Journal of Health
Services Research & Policy* 19 (4): 245–52.
<https://doi.org/10.1177/1355819614534836>.

</div>

<div id="ref-mcmichaelClimateChangeTime2009">

McMichael, AJ, M Neira, R Bertollini, D Campbell-Lendrum, and S Hales.
2009. “Climate Change: A Time of Need and Opportunity for the Health
Sector.” *The Lancet* 374 (9707): 2123–5.
<https://doi.org/10.1016/S0140-6736(09)62031-6>.

</div>

<div id="ref-nansaiCarbonFootprintJapanese2020">

Nansai, Keisuke, Jacob Fry, Arunima Malik, Wataru Takayanagi, and Naoki
Kondo. 2020. “Carbon Footprint of Japanese Health Care Services from
2011 to 2015.” *Resources, Conservation and Recycling* 152 (January):
104525. <https://doi.org/10.1016/j.resconrec.2019.104525>.

</div>

<div id="ref-nhsenglandNHSEnglandCarbon2009">

NHS England, SDU, and SEI. 2009. “NHS England Carbon Emissions. Carbon
Footprinting Report. September 2008 (Updated August 2009).” n.p.: NHS
England, Sustainable Development Unit, Stockholm Environment Institut.

</div>

<div id="ref-nhsSavingCarbonImproving2009">

NHS, and SDU. 2009. “Saving Carbon. Improving Health. NHS Carbon
Reduction Strategy for England.” Cambridge, UK: NHS England, Sustainable
Development Unit.

</div>

<div id="ref-nhsNHSEnglandCarbon2012">

NHS, and SDU. 2012. “NHS England Carbon Footprint (Published 2012).”
n.p.: NHS England, Sustainable Development Unit.

</div>

<div id="ref-oecdTacklingWastefulSpending2017">

OECD. 2017a. *Tackling Wasteful Spending on Health*. Paris: OECD
Publishing. <https://doi.org/10.1787/9789264266414-en>.

</div>

<div id="ref-oecdHealthGlance20172017">

———. 2017b. *Health at a Glance 2017: OECD Indicators*. Health at a
Glance. OECD Publishing.
<https://doi.org/10.1787/health_glance-2017-en>.

</div>

<div id="ref-oecdOECDHealthStatistics2018">

———. 2018. “OECD Health Statistics 2017.” Text. 2018.
<https://www.oecd-ilibrary.org/social-issues-migration-health/data/oecd-health-statistics_health-data-en>.

</div>

<div id="ref-oecdSystemHealthAccounts2017">

OECD, Eurostat, and WHO. 2017. “A System of Health Accounts 2011:
Revised Edition.” Paris: OECD Publishing.

</div>

<div id="ref-parvatkerCradletoGateGreenhouseGas2019">

Parvatker, Abhijeet G., Huseyin Tunceroglu, Jodi D. Sherman, Philip
Coish, Paul Anastas, Julie B. Zimmerman, and Matthew J. Eckelman. 2019.
“Cradle-to-Gate Greenhouse Gas Emissions for Twenty Anesthetic Active
Pharmaceutical Ingredients Based on Process Scale-up and Process Design
Calculations.” *ACS Sustainable Chemistry & Engineering*, March.
<https://doi.org/10.1021/acssuschemeng.8b05473>.

</div>

<div id="ref-pichlerInternationalComparisonHealth2019">

Pichler, Peter-Paul, Ingram S. Jaccard, Ulli Weisz, and Helga Weisz.
2019. “International Comparison of Health Care Carbon Footprints.”
*Environmental Research Letters* 14 (6): 064004.
<https://doi.org/10.1088/1748-9326/ab19e1>.

</div>

<div id="ref-reapSurveyUnresolvedProblems2008">

Reap, John, Felipe Roman, Scott Duncan, and Bert Bras. 2008. “A Survey
of Unresolved Problems in Life Cycle Assessment: Part 1: Goal and Scope
and Inventory Analysis.” *The International Journal of Life Cycle
Assessment* 13 (4): 290–300.
<https://doi.org/10.1007/s11367-008-0008-x>.

</div>

<div id="ref-sainiAddressingOveruseUnderuse2017">

Saini, Vikas, Shannon Brownlee, Adam G Elshaug, Paul Glasziou, and Iona
Heath. 2017. “Addressing Overuse and Underuse Around the World.” *The
Lancet* 390 (10090): 105–7.
<https://doi.org/10.1016/S0140-6736(16)32573-9>.

</div>

<div id="ref-salasClimateChangeThreatens2019">

Salas, Renee N., and Ashish K. Jha. 2019. “Climate Change Threatens the
Achievement of Effective Universal Healthcare.” *BMJ* 366 (September):
l5302. <https://doi.org/10.1136/bmj.l5302>.

</div>

<div id="ref-sandozgmbhSustainabilityReport20172017">

Sandoz GmbH,. 2017. “Sustainability Report 2017 with Integrated
Environmental Statement. Sandoz GmbH for the Kundl and Schaftenau
Plants. Updated Data to 2016.” Kundl, Austria: Sandoz, Novartis.

</div>

<div id="ref-schweitzerPharmaceuticalEconomicsPolicy2018a">

Schweitzer, Stuart O., and Z. John Lu. 2018. *Pharmaceutical Economics
and Policy: Perspectives, Promises, and Problems*. Third edition. New
York, NY: Oxford University Press.

</div>

<div id="ref-sduSustainableResilientHealthy2014a">

SDU. 2014. “Sustainable, Resilient, Healthy People & Places. A
Sustainable Development Strategy for the NHS, Public Health and Social
Care System.” Cambridge: NHS England, Sustainable Development Unit,
Public Health England.

</div>

<div id="ref-sduCarbonUpdateHealth2016">

SDU. 2016. “Carbon Update for the Health and Care Sector in England
2015.” Cambridge, UK: NHS England, Sustainable Development Unit.

</div>

<div id="ref-sduCarbonHotspotReducing2019">

———. 2019. “Carbon Hotspot: Reducing Inhalers.” Sustainable Development
Unit. 2019.
<https://www.google.com/search?client=firefox-b-d&q=Carbon+hotspot%3A%0Breducing+inhalers>.

</div>

<div id="ref-sduIdentifyingHighGreenhouse2014">

SDU, and ERM. 2014. “Identifying High Greenhouse Gas Intensity
Prescription Items for NHS in England. Final Report.” n.p.: Sustainable
Development Unit, Environmental Resources Management.

</div>

<div id="ref-sduIdentifyingHighGreenhouse2017">

———. 2017. “Identifying High Greenhouse Gas Intensity Procured Items for
the NHS in England.” n.p.: Sustainable Development Unit, Environmental
Resources Management.

</div>

<div id="ref-shermanLifeCycleGreenhouse2012">

Sherman, Jodi, Cathy Le, Vanessa Lamers, and Matthew J. Eckelman. 2012.
“Life Cycle Greenhouse Gas Emissions of Anesthetic Drugs:” *Anesthesia
& Analgesia* 114 (5): 1086–90.
<https://doi.org/10.1213/ANE.0b013e31824f6940>.

</div>

<div id="ref-tammaLetGamesBegin2014">

Tamma, Pranita D, and Sara E Cosgrove. 2014. “Let the Games Begin: The
Race to Optimise Antibiotic Use.” *The Lancet Infectious Diseases* 14
(8): 667–68. <https://doi.org/10.1016/S1473-3099(14)70809-6>.

</div>

<div id="ref-thielEnvironmentalImpactsSurgical2015">

Thiel, Cassandra L., Matthew Eckelman, Richard Guido, Matthew
Huddleston, Amy E. Landis, Jodi Sherman, Scott O. Shrake, Noe
Copley-Woods, and Melissa M. Bilec. 2015. “Environmental Impacts of
Surgical Procedures: Life Cycle Assessment of Hysterectomy in the United
States.” *Environmental Science & Technology* 49 (3): 1779–86.
<https://doi.org/10.1021/es504719g>.

</div>

<div id="ref-tomsonReducingCarbonFootprint2015">

Tomson, Charlie. 2015. “Reducing the Carbon Footprint of Hospital-Based
Care.” *Future Hospital Journal* Vol.2 (No 1: 57–62).

</div>

<div id="ref-ubaArzneimittelruckstandeUmwelt2016">

UBA. 2016. “Arzneimittelrückstände in Der Umwelt.” REPORT REP-0573.
Wien: Umweltbundesamt.

</div>

<div id="ref-ubaBerechnungTreibhausgasTHG2017">

UBA. 2017. “Berechnung von Treibhausgas (THG)-Emissionen Verschiedener
Energieträger.” Umweltbundesamt. 2017.
<http://www5.umweltbundesamt.at/emas/co2mon/co2mon.html>.

</div>

<div id="ref-ubaKlimaschutzbericht20182018">

———. 2018. “Klimaschutzbericht 2018.” REPORT REP-0 66. Wien:
Umweltbundesamt.
<https://www.umweltbundesamt.at/fileadmin/site/publikationen/REP0660.pdf>.

</div>

<div id="ref-ubaAustriaNationalInventory2019">

———. 2019. “Austria’s National Inventory Report 2019. Submission Under
the United Nations Framework Convention on Climate Changeand Under the
Kyoto Protocol.” REP-0677. Submission Under the United Nations Framework
Convention on Climate Change and Under the Kyoto Protocol. Vienna:
Umweltbundesamt (Environmental Agency Austria).
<https://www.umweltbundesamt.at/fileadmin/site/publikationen/REP0677.pdf>.

</div>

<div id="ref-unfccGreenhouseGasInventory2018">

UNFCC. 2018. “Greenhouse Gas Inventory Data - Detailed Data by Party.”
Data base. 2018. <http://di.unfccc.int/detailed_data_by_party>.

</div>

<div id="ref-usubharatanaCarbonFootprintsRubber2018">

Usubharatana, P., and H. Phungrassami. 2018. “Carbon Footprints of
Rubber Products Supply Chains (Fresh Latex to Rubber Glove).” *Applied
Ecology and Environmental Research* 16 (2): 1639–57.
<https://doi.org/10.15666/aeer/1602_16391657>.

</div>

<div id="ref-venkateshCarbonFootprintCost2016">

Venkatesh, Rengaraj, Suzanne W. van Landingham, Ashish M. Khodifad,
Aravind Haripriya, Cassandra L. Thiel, Pradeep Ramulu, and Alan L.
Robin. 2016. “Carbon Footprint and Cost–Effectiveness of Cataract
Surgery.” *Current Opinion in Ophthalmology* 27 (1): 82–88.
<https://doi.org/10.1097/ICU.0000000000000228>.

</div>

<div id="ref-vermeulenClimateChangeFood2012">

Vermeulen, Sonja J., Bruce M. Campbell, and John S.I. Ingram. 2012.
“Climate Change and Food Systems.” *Annual Review of Environment and
Resources* 37 (1): 195–222.
<https://doi.org/10.1146/annurev-environ-020411-130608>.

</div>

<div id="ref-voglerMedicinesDiscardedHousehold2014">

Vogler, Sabine, Christine Leopold, Christel Zuidberg, and Claudia Habl.
2014. “Medicines Discarded in Household Garbage: Analysis of a
Pharmaceutical Waste Sample in Vienna.” *Journal of Pharmaceutical
Policy and Practice* 7 (1). <https://doi.org/10.1186/2052-3211-7-6>.

</div>

<div id="ref-vollmerModernInhalationAnesthetics2015">

Vollmer, Martin K., Tae Siek Rhee, Matt Rigby, Doris Hofstetter,
Matthias Hill, Fabian Schoenenberger, and Stefan Reimann. 2015. “Modern
Inhalation Anesthetics: Potent Greenhouse Gases in the Global
Atmosphere.” *Geophysical Research Letters* 42 (5): 1606–11.
<https://doi.org/10.1002/2014GL062785>.

</div>

<div id="ref-walpoleSustainableHealthcareEducation2019">

Walpole, Sarah Catherine, Stefi Barna, Janet Richardson, and
Hanna-Andrea Rother. 2019. “Sustainable Healthcare Education:
Integrating Planetary Health into Clinical Education.” *The Lancet
Planetary Health* 3 (1): e6–e7.
<https://doi.org/10.1016/S2542-5196(18)30246-8>.

</div>

<div id="ref-wattsLancetCountdownTracking2017">

Watts, Nick, W Neil Adger, Sonja Ayeb-Karlsson, Yuqi Bai, Peter Byass,
Diarmid Campbell-Lendrum, Tim Colbourn, et al. 2017. “The Lancet
Countdown: Tracking Progress on Health and Climate Change.” *The Lancet*
389 (10074): 1151–64. <https://doi.org/10.1016/S0140-6736(16)32124-9>.

</div>

<div id="ref-watts2018ReportLancet2018">

Watts, Nick, Markus Amann, Nigel Arnell, Sonja Ayeb-Karlsson, Kristine
Belesova, Helen Berry, Timothy Bouley, et al. 2018. “The 2018 Report of
the Lancet Countdown on Health and Climate Change: Shaping the Health of
Nations for Centuries to Come.” *The Lancet*, November.
<https://doi.org/10.1016/S0140-6736(18)32594-7>.

</div>

<div id="ref-watts2019ReportLancet2019">

Watts, Nick, Markus Amann, Nigel Arnell, Sonja Ayeb-Karlsson, Kristine
Belesova, Maxwell Boykoff, Peter Byass, et al. 2019. “The 2019 Report of
the Lancet Countdown on Health and Climate Change: Ensuring That the
Health of a Child Born Today Is Not Defined by a Changing Climate.” *The
Lancet* 394 (10211): 1836–78.
<https://doi.org/10.1016/S0140-6736(19)32596-6>.

</div>

<div id="ref-wbgClimateSmartHealthcareLowCarbon2017">

WBG. 2017. “Climate-Smart Healthcare. Low-Carbon and Resilience
Strategies for the Health Sector.” Investing in Climate Change and
Health Series. Washington: The World Bank Group, International Bank for
Reconstruction and Development / The World Bank.

</div>

<div id="ref-weiszZurArbeitNatur2016">

Weisz, Ulli. 2016. “Zur Arbeit an Der Natur Im Krankenhaus. Perspektiven
Nachhaltiger Krankenbehandlung.” In *Nachhaltige Arbeit: Soziologische
Beiträge Zur Neubestimmung Der Gesellschaftlichen Naturverhältnisse*,
edited by Thomas Barth, Georg Jochum, and Beate Littig, 267–88.
International Labour Studies, Band 13. Frankfurt: Campus Verlag.

</div>

<div id="ref-weiszSustainableHospitalsSocioEcological2011">

Weisz, Ulli, Willi Haas, Jürgen M. Pelikan, and Hermann Schmied. 2011.
“Sustainable Hospitals: A Socio-Ecological Approach.” *GAIA -
Ecological Perspectives for Science and Society* 20 (3): 191–98.

</div>

<div id="ref-wernetLifeCycleAssessment2010">

Wernet, Gregor, Sarah Conradt, Hans Peter Isenring, Concepción
Jiménez-González, and Konrad Hungerbühler. 2010. “Life Cycle Assessment
of Fine Chemical Production: A Case Study of Pharmaceutical Synthesis.”
*The International Journal of Life Cycle Assessment* 15 (3): 294–303.
<https://doi.org/10.1007/s11367-010-0151-z>.

</div>

<div id="ref-whoCOP24SpecialReport2018">

WHO. 2018. “COP24 Special Report. Health & Climate Change.” Licence: CC
BY-NC-SA 3.0 IGO. Geneva: World Health Organization,

</div>

<div id="ref-wilkinsonCostsSwitchingLow2019">

Wilkinson, Alexander J K, Rory Braggins, Ingeborg Steinbach, and James
Smith. 2019. “Costs of Switching to Low Global Warming Potential
Inhalers. An Economic and Carbon Footprint Analysis of NHS Prescription
Data in England.” *BMJ Open* 9 (10): e028763.
<https://doi.org/10.1136/bmjopen-2018-028763>.

</div>

<div id="ref-willettFoodAnthropoceneEAT2019">

Willett, Walter, Johan Rockström, Brent Loken, Marco Springmann, Tim
Lang, Sonja Vermeulen, Tara Garnett, et al. 2019. “Food in the
Anthropocene: The EAT–Lancet Commission on Healthy Diets from
Sustainable Food Systems.” *The Lancet* 393 (10170): 447–92.
<https://doi.org/10.1016/S0140-6736(18)31788-4>.

</div>

<div id="ref-wuCarbonFootprintChinese2019">

Wu, Rui. 2019. “The Carbon Footprint of the Chinese Health-Care System:
An Environmentally Extended Input–Output and Structural Path Analysis
Study.” *The Lancet Planetary Health* 3 (10): e413–e419.
<https://doi.org/10.1016/S2542-5196(19)30192-5>.

</div>

</div>
