This R Markdown file contains all code and instructions to reproduce the
results, tables, and data underlying the figures shown in (TODO:link to
pub). It’s output is an `xlsx` data file (AUTHCF SI\_data.xlsx) which
contains all results used in the main manuscript and the supplementary
information of the publication. The manuscript itself was written in
RMarkdown and figures, tables and references to values are generated
automatically from this data file.

# R packages: install and load required R packages

``` r
if (!require("pacman")) install.packages("pacman")
pacman::p_load(readxl,
               here,
               wbstats,
               openxlsx,
               tidyverse)
```

# External data files

This section contains instructions on how to download all external data
files required to run the scripts that produced the results of our
analysis. To fully reproduce our health carbon footprin analysis an
operational copy of the [Eora](https://worldmrio.com/) EE-MRIO model is
needed, which in turn required substantial computing resources to run.
For this reason, the R script running the EE-MRIO calculations
(`code/eemrio_hcf.R`) is external to this document which should still
run even without the resources to run Eora.

The main external input data is connected to health expenditure data
from either the OECD or the Austrian statistical office. Unfortunately,
accessing this data is likely a fairly brittle process. None of it has
permalinks or even stable versions. We have attempted do describe as
robust a process to aquire these data as we could below. If the data are
no longer available at these locations, or have changed in a way that
breaks this document, please [contact us](mailto:pichler@pik-potsdam.de)
and we will provide our original files.

## OECD SHA health care expenditure data

Download [OECD health care expenditure data](https://stats.oecd.org/)
from the OECD health statistics database and navigate to: `Health` -\>
‘Health expenditure and financing’ -\> ‘Health expenditure and
financing’. Make the following selections:

  - Financing scheme: click on -\>|Financing scheme and select
    “Government/compulsory schemes” and “Voluntary schemes/household
    out-of-pocket payments”
  - Function: Current expenditure on health (all functions)
  - Provider: click on -\>|Provider and select each individual provider
  - Measure: click on -\>|Measure and select “Current prices” and
    “Constant prices, OECD base year” - Year: click on -\>|Year and
    select year from 2005 to 2015

`Export` as `Text file (CSV)`, copy to this project’s data folder and
enter filename in the code snipped below:

``` r
# OECD Health expenditure data
#oecd_sha_filepath = normalizePath(paste0(here(),"/data/","SHA_04032020144738999.csv"))
oecd_sha_filepath = normalizePath(paste0(here(),"/data/","OECD_healthexpenditures.csv"))
```

## OECD health investments data

To download the [OECD health investment data](https://stats.oecd.org/)
navigate to -\> `Health` -\> `Health expenditure and financing` -\>
`Gross fixed capital formation in the health care system` and select:

  - Measure: click on -\>|Measure and select “Current prices” and
    “Constant prices, OECD base year” - Year: click on -\>|Year and
    select year from 2005 to 2015

`Export` as `Text file (CSV)`, copy to this project’s data folder and
enter filename in the code snipped below:

``` r
# OECD Investment data
#oecd_inv_filepath = normalizePath(paste0(here(),"/data/","SHA_HK_04032020154113152.csv"))
oecd_inv_filepath = normalizePath(paste0(here(),"/data/","OECD_healthinvestment.csv"))
```

## Statistics Austria health care expenditure data

To download the [health care expenditure data from Statistik
Austria](https://www.statistik.at/web_de/statistiken/menschen_und_gesellschaft/gesundheit/gesundheitsausgaben/index.html)
look for an xlsx file *“Gesundheitsausgaben in Österreich laut System of
Health Accounts (SHA) 1990 - 2018”* (yearly updates should not break the
script). This data is equvalent to the OECD data but additionally breaks
investments (‘gross fixed capital formation’) down into public and
private.

Download the Excel file into this prohect’s data folder correcting the
filename if necessary in the code snippet below:

``` r
# The file exists in english and in german, specify here which version you are using
# option are  "EN" or "DE"
investments_aut_stat_language = "EN"

# Austrian health expenditure and investments data
at_stat_sha_filepath = normalizePath(
  paste0(here(),
         "/data/",
        # "gesundheitsausgaben_in_oesterreich_laut_system_of_health_accounts_sha_1990.xlsx"))
        "Statistik_Austria_healthexpenditures_and_investment.xlsx"))
```

## IMF exchange rates file

The interface of the IMF website currently does not allow a bulk
download of yearly average exchange rates. For this study, we have
manually created a datafile using the [IMF Exchange Rate Archives by
Month Query
Tool](https://www.imf.org/external/np/fin/data/param_rms_mth.aspx) and
averaging over each year to get exchange rates expressed in annual
terms. The file is therefore provided in the repository
(`IMF_exchange_rates_annual.csv`).

``` r
# IMF exchange rate file
imf_ex_filepath = normalizePath(paste0(here(),"/data/","IMF_exchange_rates_annual.csv"))
```

## World Bank population data

The world bank population data (used for per capita indicator
calculation) is accessed dynamically using the
[wbstats](https://cran.r-project.org/web/packages/wbstats/index.html)
package.

# Loading and tidying external data

This section loads and prepares the external data for use in the
analysis. This involves mostly selecting and cleaning the necessary
data. The IMF exchange rates are used to convert OECD local currency
expenditure data into current prices USD for use in the EE-MRIO
calculation.

``` r
# Read in OECD health care expenditure data 
# Filter for Austria, the years 2005 to 2015, current prices and 
# current (public + private) health expenditure
# of each provider category:
hexp_oecd = read_csv(oecd_sha_filepath) %>% 
  filter(Measure == "Current prices", 
           Function == "Current expenditure on health (all functions)", 
           !(`Financing scheme` == "All financing schemes"), 
         !(Provider == "All providers")) %>% 
  rename(Financing.scheme = `Financing scheme`) %>%
  select(Financing.scheme, Provider, Measure, LOCATION, Year, Unit, Value) %>%
    filter(Year %in% c("2005":"2015"), LOCATION == "AUT")


# Load austrian investment data
investments_aut_stat = read_excel(at_stat_sha_filepath, skip=1) %>%
    rename(category = names(.)[1])

  
if (investments_aut_stat_language == "EN") {
  investments_aut_stat = investments_aut_stat %>%
  filter(category %in% c("Public gross capital formation in health care provider industries",
                         "Private gross capital formation in health care provider industries")) %>%
  mutate(Financing.scheme = if_else(
    category == "Public gross capital formation in health care provider industries",
    "Government/compulsory schemes", "Voluntary schemes/household out-of-pocket payments")) 
} else if (investments_aut_stat_language == "DE") {
  investments_aut_stat = investments_aut_stat %>%
  filter(category %in% c("Investitionen (öffentlich)","Investitionen (privat)")) %>%
  mutate(Financing.scheme = if_else(category == "Investitionen (öffentlich)",
                                    "Government/compulsory schemes",
                                    "Voluntary schemes/household out-of-pocket payments"))
} else {
  print("not a recognized language")
}

investments_aut_stat = investments_aut_stat %>%
  select(-category) %>%
  pivot_longer(-Financing.scheme, names_to = "Year", values_to = "Value") %>%
  mutate(Year = strtoi(Year)) %>%
  mutate(Provider = "Gross fixed capital formation (all types of assets)",
         Measure = "Current prices",
         LOCATION = "AUT",
         Unit = "Euro") %>%
  select(Financing.scheme, Provider, Measure, LOCATION, Year, Unit, Value)


## bind investments from statsitics austria to OECD health expenditure
health_expenditure = hexp_oecd  %>%
  bind_rows(investments_aut_stat)


# load IMF historical exchange rates and join to expenditures:
IMF_exchange_rates = read_csv(imf_ex_filepath) 

# convert local currency units to current USD using IMF exchange rates
health_expenditure = health_expenditure %>% 
  left_join(IMF_exchange_rates, by = c("Year", "Unit")) %>% 
  mutate(Current_prices_USD = Value / EX_rate)
```

# EE-MRIO calculation of mapped CO2 intensities and national carbon footprint

The code to calculate the mapped CO2 intensities for Austria needs a lot
of resources and so was run on our high performance cluster. To
reproduce the results run the script `code/eemrio_hcf.R` in this
project’s code folder. It first creates a function which takes a
manually created concordance matrix
(`data/Concordance_matrix_AUT_provider.csv`) mapping the Eora
productions sectors to the 20 SHA health expenditure categories, and
returns the mapped CO2 intensities of the SHA expenditure categories.
Then it loops through each study year, performing the standard
input-output calculation with the full version of Eora to calculate the
unmapped CO2 intensities of every Eora production sector in base price
for that year. Then the concordance to mapped intensities function is
called for each year.

The Eora data files are all accessed at the [Eora
website](https://worldmrio.com), in the ‘Download files’ section of the
‘Full Eora’ page. 7 tables are needed from each study year:

  - Technology (T) matrix
  - Final Demand (Y) matrix
  - Trade Margin on Final Demand\*
  - Transport Margin on Final Demand\*
  - Taxes Margin on Final Demand\*
  - Subsidies Margin on Final Demand\*
  - Satellite Account (Q)

\*Note: if you have trouble accessing the the trade, transport, taxes
and subsidies margins ON FINAL DEMAND tables, feel free to [contact
us](jaccard@pik-potsdam.de).

Finally, some manually created .csv files are used for indexing Eora
sectors, mapping and grouping. These can be found in the ‘data’ folder
(along with the concordance matrix):

  - ‘fd\_and\_t\_ids.csv’
  - ‘trade\_ids.csv’
  - ‘transport\_ids.csv’
  - ‘Concordance\_matrix\_AUT\_provider.csv’

The script returns two files which are required for the subsequent
calculations in this document:

  - `Fully_mapped_intensities_AUT.csv`: the mapped upstream CO2
    intensities calculated using the Eora EE-MRIO model.

  - `cf_aut.csv`: the total national carbon footprint of Austria between
    2005 and 2015 calculated with the Eora EE-MRIO model.

Our pre-calculated version of both files are located in the data folder.

``` r
# EE-MRIO result file 
mapped_intensities_filepath = normalizePath(paste0(here(),
                                                   "/data/",
                                                   "Fully_mapped_intensities_AUT.csv"))

# Read in Austrian mapped intensities saved in previous code chunk:
Fully_mapped_intensities_CO2_provider = read_csv(mapped_intensities_filepath) %>% 
  rename(CO2 = "rep.TIV_country_trade..length.percent_trade..")
```

# Generate data file for manuscript and supplementary information

Join health care expenditure data with the mapped intensities to create
main dataframe ‘df\_hcf’.

``` r
# Join CO2 intensities to expenditure data:
df_hcf = health_expenditure %>% 
  filter(Year<=2015, Year>=2005) %>%
  left_join(Fully_mapped_intensities_CO2_provider, 
            by = c('Financing.scheme','Provider','LOCATION','Year'))
```

### HCF summary

This requires extra data files including the national carbon footprint
calculated with Eora, World Bank population data, and health care
expenditure in constant 2010 prices. These are each joined to the main
dataframe after they are loaded and cleaned appropriately.

``` r
# hcf summary

df_hcf_summarised = df_hcf %>% 
  filter(Year<=2015, Year>=2005) %>%
  mutate(hcf_kt = (Current_prices_USD*1000000) * (CO2/1000))  %>%
  group_by(Year) %>%
  summarise(hcf_kt = sum(hcf_kt)) 
```

National carbon footprints are calulated using the full version of Eora
(see above). Join saved national footprints and World Bank population
data to ‘df\_hcf\_summarised’.

``` r
cf_aut_filepath = normalizePath(paste0(here(),
                                       "/data/",
                                       "cf_aut.csv"))

# Join saved national footprints and World Bank population data to 'df_hcf_summarised'. 

# Read in saved national footprints calculated in previous code chunk
# and filter for Austria:
cf_aut = read_csv(cf_aut_filepath) %>%
  rename(national_cf_kt = National_indirect_CO2_footprint_Gg)

df_hcf_summarised = df_hcf_summarised %>% 
  left_join(cf_aut, by = 'Year')

# Read in population data from the World Bank
# and filter for Austria:
df_population = wb(indicator = "SP.POP.TOTL", startdate = 2005, enddate = 2015) %>%
  filter(iso3c == "AUT") %>%
  mutate(Year = strtoi(date)) %>%
  select(LOCATION = iso3c,
         population = value,
         Year)

df_hcf_summarised = df_hcf_summarised %>% 
  left_join(df_population, by = 'Year')
```

Read in health expenditure data from the OECD health statistics database
in the same way as before, but now filter for Austrian health
expenditure expressed in ‘Constant prices, OECD base year.’ Do this for
total investments as well using the OECD investment data (does not need
to be broken down by public and private). The OECD base year is 2010.
Join to ‘df\_hcf\_summarised’.

``` r
# Health care expenditure in constant 2010 USD. Same procedure as before but 
# now filter OECD 
# current and investment health care expenditure for constant prices 2010 
# base year before converting from euro to USD.

# Health expenditure in constant prices, filter
# for Austria and the years 2005 to 2015 as well:
hexp_oecd_const = read_csv(oecd_sha_filepath) %>% 
  rename(Financing.scheme = `Financing scheme`) %>%
  filter(Measure == "Constant prices, OECD base year", 
         Function == "Current expenditure on health (all functions)", 
         !(Financing.scheme == "All financing schemes"), 
         !(Provider == "All providers")) %>%
  select(Financing.scheme, Provider, Measure, LOCATION, Year, Unit, Value) %>%
    filter(Year %in% c("2005":"2015"), LOCATION == "AUT")

# OECD health care investment expenditure data
# in constant prices, filter for Austria and the 
# years 2005 to 2015 as well:
h_inv_const = read_csv(oecd_inv_filepath) %>% 
  filter(Measure == "Constant prices, OECD base year") %>% 
  rename(Financing.scheme = "Provider", Provider = "Type of asset") %>% 
  filter(Year %in% c("2005":"2015"), LOCATION == "AUT") %>%
  mutate(Unit = "Euro") %>%
  select(Financing.scheme, Provider, Measure, LOCATION, Year, Unit, Value)
    

# Bind investments to current expenditure:
hexp_const_w_inv= hexp_oecd_const %>% 
  bind_rows(h_inv_const)

# Sum over each year: 
hexp_const = hexp_const_w_inv %>% 
  left_join(IMF_exchange_rates %>%
              filter(Year >=2005, Year<=2015), by = c("Year", "Unit")) %>% 
  mutate(hexp_const2010_USD = Value / EX_rate) %>%
  group_by(Year) %>% 
  summarise(hexp_const2010_lcu = sum(Value),
    hexp_const2010_USD = sum(hexp_const2010_USD))

hexp_const_AUT = hexp_const %>%
  rename(hexp_const_2010_million_EUR = hexp_const2010_lcu)

# Join to 'df_hcf_summarised'

df_hcf_summarised = df_hcf_summarised %>% 
  left_join(hexp_const_AUT, by = 'Year')
```

Create ‘HCF\_summary’ dataframe.

``` r
HCF_summary = df_hcf_summarised %>% 
  mutate(hcf_t_per_capita = (hcf_kt/population)*1000) %>%
  select(Year,hcf_kt,national_cf_kt,hcf_t_per_capita,hexp_const_2010_million_EUR)
```

### Figure 1a

``` r
Fig1a_data = df_hcf %>%
  mutate(Provider = dplyr::recode(Provider,
        "Gross fixed capital formation (all types of assets)" = "Investment",
         "Hospitals" = "Hospitals",
         "Providers of ambulatory health care" = "Ambulatory",
        "Retailers and other providers of medical goods" = "Retail",
         "Providers of ancillary services" = "Other",
       "Providers of health care system administration and financing"  = "Other",
         "Providers of preventive care"  = "Other",
        "Residential long-term care facilities"  = "Other",
         "Rest of the economy"  = "Other",
         "Rest of the world"  = "Other")) %>%
  mutate(ktCO2 = (Current_prices_USD*1000000) * (CO2/1000)) %>%
  group_by(Year,Provider) %>%
  summarise(ktCO2 = sum(ktCO2)) %>%
  select(Year,Provider,ktCO2) 
```

### Figure 1b

``` r
Fig1b_data = hexp_const_AUT %>%
  rename(hexp_const2010_EUR = hexp_const_2010_million_EUR) %>%
  gather(indicator,expenditure,2:3)
```

### Table S2

``` r
Tab_S2 = df_hcf %>%
  filter(Year %in% c("2005","2010","2014","2015")) %>%
  mutate(ktCO2 = (Current_prices_USD*1000000) * (CO2/1000)) %>%
  rename(Current_expenditure_million_EUR = Value,
         Current_expenditure_million_USD = Current_prices_USD) %>%
  select(Year,Financing.scheme,
         Provider,Current_expenditure_million_EUR, 
         Current_expenditure_million_USD,ktCO2)
```

### Figure 2

``` r
Fig2_data = data.frame(stringsAsFactors=FALSE,
        Year = c(2010L, 2010L, 2010L, 2010L, 2010L, 2010L, 2010L, 2010L, 2010L,
                 2010L, 2010L, 2010L, 2010L, 2010L, 2010L, 2010L, 2010L,
                 2010L),
    Provider = c("Hospitals", "Hospitals", "Hospitals", "Hospitals",
                 "Hospitals", "Hospitals", "Hospitals", "Hospitals",
                 "Medical Retail", "Medical Retail", "Other", "Other", "Other", "Other",
                 "Other", "Ambulatory", "Ambulatory", "Investment"),
    Category = c("Pharmaceuticals", "Medical G&S", "Other G&S",
                 "Food & Catering", "Facility Services", "Energy Fossil",
                 "Energy Electricity", "Energy Dist. Heat", "Pharmaceuticals",
                 "Other medical", "Ancillary", "Administration", "Preventive",
                 "Long-Term", "Other", "G&S", "Energy", "Investment"),
       ktCO2 = c(472.6699099635, 890.7369366095, 77.1685531151, 96.592045576,
                 171.2785571984, 144.1117194333, 367.3560879125,
                 254.5220989687, 1132.2826498839, 429.1439135402, 208.9921509648,
                 142.1269278984, 55.1520322635, 580.7897303339, 512.9102456708,
                 1229.3777169848, 158.0902400688, 719.4608007318)
)
```

### Figure 3

``` r
Fig3_data = data.frame(stringsAsFactors=FALSE,
          Year = c(2005L, 2005L, 2005L, 2005L, 2005L, 2005L, 2005L, 2005L,
                   2010L, 2010L, 2010L, 2010L, 2010L, 2010L, 2010L, 2010L,
                   2015L, 2015L, 2015L, 2015L, 2015L, 2015L, 2015L, 2015L),
      Provider = c("Hospitals", "Ambulatory", "Hospitals", "Hospitals",
                   "Ambulatory", "Ambulatory", "Home health care services",
                   "Patient Transportation", "Hospitals", "Ambulatory", "Hospitals",
                   "Hospitals", "Ambulatory", "Ambulatory",
                   "Home health care services", "Patient Transportation", 
                   "Hospitals", "Ambulatory",
                   "Hospitals", "Hospitals", "Ambulatory", "Ambulatory",
                   "Home health care services", "Patient Transportation"),
   Energy.Type = c("Diesel, petrol, oil and gas", "Oil and gas", "Electricity",
                   "District Heat", "Electricity", "District Heat",
                   "Diesel and petrol", "Diesel and petrol", "Diesel, petrol, oil and gas", "Oil and gas",
                   "Electricity", "District Heat", "Electricity", "District Heat",
                   "Diesel and petrol", "Diesel and petrol", "Diesel, petrol, oil and gas", "Oil and gas", "Electricity", "District Heat", "Electricity",
                   "District Heat", "Diesel and petrol", "Diesel and petrol"),
         ktCO2 = c(144.3649447743, 43.4006624393, 353.2974359316,
                   307.0688341445, 28.0934937634, 6.9160445851, 22.9588853205,
                   42.9108720067, 144.1117194333, 41.6415565708, 367.3560879125,
                   254.5220989687, 30.2443184155, 7.0087865991, 25.2745544028,
                   53.9210240806, 110.9940741995, 39.6242186604, 284.8474258115,
                   232.1724507694, 26.1969268859, 9.9949372028, 27.759622758,
                   35.9560949611)
)
```

### Figure 4

``` r
Fig4_data = data.frame(stringsAsFactors=FALSE,
        Year = c(2005L, 2005L, 2005L, 2005L, 2005L, 2010L, 2010L, 2010L, 2010L,
                 2010L, 2015L, 2015L, 2015L, 2015L, 2015L),
    Provider = c("Ambulatory", "Ambulatory", "Hospitals", "Hospitals",
                 "Hospitals", "Ambulatory", "Ambulatory", "Hospitals",
                 "Hospitals", "Hospitals", "Ambulatory", "Ambulatory", "Hospitals",
                 "Hospitals", "Hospitals"),
    Category = c("Patients", "Staff", "Patients", "Staff", "Visitors",
                 "Patients", "Staff", "Patients", "Staff", "Visitors",
                 "Patients", "Staff", "Patients", "Staff", "Visitors"),
       ktCO2 = c(415.8246756004, 47.0779532607, 132.4519905914, 83.7030205961,
                 36.621353195, 448.5256689497, 50.1168768729, 138.7667476978,
                 84.7620003524, 38.5409445159, 483.6245844494, 54.8806280008,
                 151.4149854702, 93.926152555, 40.7955817893)
)
```

### Table 1

``` r
Tab1_data = data.frame(stringsAsFactors=FALSE,
      API.Name = c("Paracetamol", "Acetylsalicylic acid", "Ibuprofen",
                   "Naproxen"),
    gCO2e_per_gAPI = c(7.8, 4.9, 3.1, 2.3),
   ktCO2e_per_year = c(0.4, 0.16, 0.1, 0.03)
)
```

### Table 2

``` r
Tab2_data = data.frame(stringsAsFactors=FALSE,
      API.Name = c("Antibiotics", "Amoxicillin"),
    gCO2e_per_gAPI = c(14.3, 14.3),
   ktCO2e_per_year = c(1, 0.3)
)
```

### Table 3

``` r
Tab3_data = data.frame(stringsAsFactors=FALSE,
    API.Name = c("Desflurane", "Sevoflurane", "Nitrous oxide (N2O)",
                 "Sum anesthetic gases", "pMDI", "Desflurane", "Sevoflurane",
                 "Nitrous oxide (N2O)", "Sum anesthetic gases", "pMDI",
                 "Desflurane", "Sevoflurane", "Nitrous oxide (N2O)",
                 "Sum anesthetic gases", "pMDI"),
    unit = c("kt CO2e/year", "kt CO2e/year","kt CO2e/year","kt CO2e/year",
             "kt CO2e/year", "kt CO2e/year", "kt CO2e/year", "kt CO2e/year", 
             "kt CO2e/year", "kt CO2e/year", "kt CO2e/year", "kt CO2e/year",
             "kt CO2e/year", "kt CO2e/year", "kt CO2e/year"),
        Year = c(2005L, 2005L, 2005L, 2005L, 2005L, 2010L, 2010L, 2010L, 2010L,
                 2010L, 2015L, 2015L, 2015L, 2015L, 2015L),
       value = c("-", "-", "-", "-", "13", "1.6", "1.2", "28.2", "31", "21.6",
                 "0.6", "1.3", "19.5", "21.4", "25.9")
)
```

### Table S3

``` r
Tab_S3 = data.frame(stringsAsFactors=FALSE,
   Consumption_category_goods_and_services = c("pharmaceuticals",
                                        "other medical goods",
                                        "medical devices (current costs)",
                                        "other non-medical goods", "food",
                                        "cleaning agents",
                                        "office supply, paper", "medical services",
                                        "non-medical services", "laundry",
                                        "catering",
                                        "cleaning services", "maintance (services)",
                                        "pharmaceuticals",
                                        "other medical goods",
                                        "medical devices (current costs)",
                                        "other non-medical goods", "food", "cleaning agents",
                                        "office supply, paper",
                                        "medical services", "non-medical services",
                                        "laundry", "catering",
                                        "cleaning services",
                                        "maintance (services)"),
        Source_or_corresponding_sector_in_Eora = 
     c("EE-MRIO results: retailers and other providers of medical goods",
      "EE-MRIO results: retailers and other providers of medical goods",
      "Eora: Medical precision and optical instruments",
      "own assumption",
      "Eora: food products and beverages",
      "Eora: chemicals", "Eora: printed matter,
        recorded material",
      "EE-MRIO results: overall health care carbon footprint", "Eora: other services",
      "Eora: other business services",
      "Eora: food products and beverages",
      "Eora: other business services",
      "Eora: other business services and construction (average)",
      "EE-MRIO results: retailers and other providers of medical goods",
      "EE-MRIO results: retailers and other providers of medical goods",
      "Eora: Medical precision and optical instruments",
      "own assumption",
      "Eora: food products and beverages",
      "Eora: chemicals", "Eora: printed matter,
      recorded material",
      "EE-MRIO results: overall health care carbon footprint", "Eora: other services",
      "Eora: other business services",
      "Eora: food products and beverages",
      "Eora: other business services",
      "Eora: other business services and construction (average)"),
                           eora_index_number = c("n.a.", "n.a.", "1307",
                                                 "n.a.", "1289", "1298",
                                                 "1296", "n.a.", "1338", "1331",
                                                 "1289", "1331", "1331, 1314", "n.a.",
                                                 "n.a.", "1307", "n.a.", "1289",
                                                 "1298", "1296", "n.a.", "1338",
                                                 "1331", "1289", "1331", "1331,
                                                 1314"),
                                        Year = c(2005L, 2005L, 2005L, 2005L,
                                                 2005L, 2005L, 2005L, 2005L,
                                                 2005L, 2005L, 2005L, 2005L, 2005L,
                                                 2010L, 2010L, 2010L, 2010L,
                                                 2010L, 2010L, 2010L, 2010L, 2010L,
                                                 2010L, 2010L, 2010L, 2010L),
                                       kg_CO2_per_Euro = c(0.4, 0.4, 0.29, 0.15, 0.4,
                                                 0.45, 0.44, 0.31, 0.19, 0.26,
                                                 0.4, 0.26, 0.36, 0.31, 0.31,
                                                 0.22, 0.15, 0.31, 0.36, 0.34, 0.24,
                                                 0.14, 0.2, 0.31, 0.2, 0.29)
)
```

### Figure S1

``` r
FigS1_data = tibble::tribble(
                          ~Consumption.categories, ~Year,                         ~indicator, ~value,
                                "Pharmaceuticals", 2005L, "CO2 intensity (kt CO2/mio. Euro)",      1,
                "Other medical non-durable goods", 2005L, "CO2 intensity (kt CO2/mio. Euro)",      1,
            "Medical durables (maintenance only)", 2005L, "CO2 intensity (kt CO2/mio. Euro)",   0.74,
                        "Other non-medical goods", 2005L, "CO2 intensity (kt CO2/mio. Euro)",   0.38,
                                           "Food", 2005L, "CO2 intensity (kt CO2/mio. Euro)",   1.01,
                                "Cleaning agents", 2005L, "CO2 intensity (kt CO2/mio. Euro)",   1.13,
                           "Office supply, paper", 2005L, "CO2 intensity (kt CO2/mio. Euro)",    1.1,
                  "Medical services (outsourced)", 2005L, "CO2 intensity (kt CO2/mio. Euro)",   0.31,
                     "Other non-medical services", 2005L, "CO2 intensity (kt CO2/mio. Euro)",   0.19,
       "Laundry (outsourced non-medical service)", 2005L, "CO2 intensity (kt CO2/mio. Euro)",   0.26,
     "Catering (outsocurced non-medical service)", 2005L, "CO2 intensity (kt CO2/mio. Euro)",    0.4,
     "Cleaning (outsourced non-medical service )", 2005L, "CO2 intensity (kt CO2/mio. Euro)",   0.26,
  "Maintenance (outsourced non-medical services)", 2005L, "CO2 intensity (kt CO2/mio. Euro)",   0.36,
                                         "Energy", 2005L, "CO2 intensity (kt CO2/mio. Euro)",   5.57,
                                "Pharmaceuticals", 2005L,                 "kt CO2 emissions", 521.25,
                "Other medical non-durable goods", 2005L,                 "kt CO2 emissions", 846.23,
            "Medical durables (maintenance only)", 2005L,                 "kt CO2 emissions",   6.57,
                        "Other non-medical goods", 2005L,                 "kt CO2 emissions",  33.06,
                                           "Food", 2005L,                 "kt CO2 emissions",  82.15,
                                "Cleaning agents", 2005L,                 "kt CO2 emissions",  12.49,
                           "Office supply, paper", 2005L,                 "kt CO2 emissions",  22.82,
                  "Medical services (outsourced)", 2005L,                 "kt CO2 emissions",  40.12,
                     "Other non-medical services", 2005L,                 "kt CO2 emissions",   36.9,
       "Laundry (outsourced non-medical service)", 2005L,                 "kt CO2 emissions",  25.73,
     "Catering (outsocurced non-medical service)", 2005L,                 "kt CO2 emissions",     28,
     "Cleaning (outsourced non-medical service )", 2005L,                 "kt CO2 emissions",  15.79,
  "Maintenance (outsourced non-medical services)", 2005L,                 "kt CO2 emissions", 101.16,
                                         "Energy", 2005L,                 "kt CO2 emissions", 804.73,
                                "Pharmaceuticals", 2010L, "CO2 intensity (kt CO2/mio. Euro)",   0.78,
                "Other medical non-durable goods", 2010L, "CO2 intensity (kt CO2/mio. Euro)",   0.78,
            "Medical durables (maintenance only)", 2010L, "CO2 intensity (kt CO2/mio. Euro)",   0.55,
                        "Other non-medical goods", 2010L, "CO2 intensity (kt CO2/mio. Euro)",   0.37,
                                           "Food", 2010L, "CO2 intensity (kt CO2/mio. Euro)",   0.77,
                                "Cleaning agents", 2010L, "CO2 intensity (kt CO2/mio. Euro)",   0.89,
                           "Office supply, paper", 2010L, "CO2 intensity (kt CO2/mio. Euro)",   0.86,
                  "Medical services (outsourced)", 2010L, "CO2 intensity (kt CO2/mio. Euro)",   0.24,
                     "Other non-medical services", 2010L, "CO2 intensity (kt CO2/mio. Euro)",   0.14,
       "Laundry (outsourced non-medical service)", 2010L, "CO2 intensity (kt CO2/mio. Euro)",    0.2,
     "Catering (outsocurced non-medical service)", 2010L, "CO2 intensity (kt CO2/mio. Euro)",   0.31,
     "Cleaning (outsourced non-medical service )", 2010L, "CO2 intensity (kt CO2/mio. Euro)",    0.2,
  "Maintenance (outsourced non-medical services)", 2010L, "CO2 intensity (kt CO2/mio. Euro)",   0.29,
                                         "Energy", 2010L, "CO2 intensity (kt CO2/mio. Euro)",   4.31,
                                "Pharmaceuticals", 2010L,                 "kt CO2 emissions",  472.7,
                "Other medical non-durable goods", 2010L,                 "kt CO2 emissions",  839.3,
            "Medical durables (maintenance only)", 2010L,                 "kt CO2 emissions",    5.2,
                        "Other non-medical goods", 2010L,                 "kt CO2 emissions",   37.4,
                                           "Food", 2010L,                 "kt CO2 emissions",   72.6,
                                "Cleaning agents", 2010L,                 "kt CO2 emissions",   11.5,
                           "Office supply, paper", 2010L,                 "kt CO2 emissions",   19.7,
                  "Medical services (outsourced)", 2010L,                 "kt CO2 emissions",   46.2,
                     "Other non-medical services", 2010L,                 "kt CO2 emissions",   39.8,
       "Laundry (outsourced non-medical service)", 2010L,                 "kt CO2 emissions",   22.2,
     "Catering (outsocurced non-medical service)", 2010L,                 "kt CO2 emissions",     24,
     "Cleaning (outsourced non-medical service )", 2010L,                 "kt CO2 emissions",   18.2,
  "Maintenance (outsourced non-medical services)", 2010L,                 "kt CO2 emissions",   99.6,
                                         "Energy", 2010L,                 "kt CO2 emissions",    766
  )
```

### Table S4

``` r
Tab_S4 = data.frame(stringsAsFactors=FALSE,
   fossil_fuel = c("Oil", "Gas", "Liquid gas", "Diesel", "Petrol",
                       "Oil", "Gas", "Liquid gas", "Diesel", "Petrol"),
   indicator = c("direct","direct","direct","direct","direct",
            "total","total","total","total","total"),
              kg.CO2e_per_kWh = c("0.27", "0.20", "0.23", "0.25", "0.25",
                         "0.34", "0.24", "0.31", "0.32", "0.33")
)
```

### Table S5

``` r
Tab_S5 = data.frame(stringsAsFactors=FALSE,
   type = c("Austrian power generation", "District heating",
                         "Austrian power generation", "District heating",
                         "Austrian power generation", "District heating"),
                Year = c(2005L, 2005L, 2010L, 2010L, 2015L, 2015L),
               kg.CO2e_per_kWh = c(385, 0.27, 364, 0.21, 302, 0.21)
)
```

### Table S6

``` r
Tab_S6 = data.frame(stringsAsFactors=FALSE,
    vehicle.type = c("Light duty vehicles", "Light duty vehicles",
                     "Personal cars", "Personal cars", "Bus", "Bus",
                     "Metro/Tram", "Train", "Train", "Light duty vehicles",
              "Light duty vehicles", "Personal cars", "Personal cars", "Bus", "Bus",
                     "Metro/Tram", "Train", "Train", "Light duty vehicles",
                     "Light duty vehicles", "Personal cars", "Personal cars", "Bus",
                     "Bus", "Metro/Tram", "Train", "Train"),
   emission.type = c("direct emissions", "total emissions", "direct emissions",
                     "total emissions", "direct emissions", "total emissions",
                     "total emissions", "direct emissions", "total emissions",
                     "direct emissions", "total emissions", "direct emissions",
                     "total emissions", "direct emissions", "total emissions",
                     "total emissions", "direct emissions", "total emissions",
                     "direct emissions", "total emissions", "direct emissions",
                     "total emissions", "direct emissions", "total emissions",
                     "total emissions", "direct emissions", "total emissions"),
            unit = c("g CO2e/km", "g CO2e/km", "g CO2e/km", "g CO2e/km",
                     "g CO2e/km/person", "g CO2e/km/person",
                     "g CO2e/km/person", "g CO2e/km/person", "g CO2e/km/person", 
                     "g CO2e/km",
                     "g CO2e/km", "g CO2e/km", "g CO2e/km", "g CO2e/km/person",
                     "g CO2e/km/person", "g CO2e/km/person", "g CO2e/km/person",
                     "g CO2e/km/person", "g CO2e/km", "g CO2e/km", "g CO2e/km",
                     "g CO2e/km", "g CO2e/km/person", "g CO2e/km/person",
                     "g CO2e/km/person", "g CO2e/km/person", "g CO2e/km/person"),
            Year = c(2005L, 2005L, 2005L, 2005L, 2005L, 2005L, 2005L, 2005L,
                     2005L, 2010L, 2010L, 2010L, 2010L, 2010L, 2010L, 2010L,
                     2010L, 2010L, 2015L, 2015L, 2015L, 2015L, 2015L, 2015L, 2015L,
                     2015L, 2015L),
           value = c(270L, 325L, 158L, 206L, 47L, 56L, 22L, 8L, 14L, 270L,
                     325L, 158L, 206L, 47L, 56L, 22L, 8L, 15L, 196L, 244L,
                     167L, 212L, 41L, 48L, 22L, 9L, 13L)
)
```

### Table S10

``` r
Tab_S10 = data.frame(stringsAsFactors=FALSE,
                     medical.glove = c("Latex glove","","Sum",
                                       "Nitril glove","","Sum"),
                     material_processing = c("Latex","glove production","",
                                             "Nitril","glove production",""),
                     g_CO2e_per_gram_of_product = c(2.31,1.01,
                                                     3.32,10.66,1.01,
                                                     11.67)
)
```

### Table S15

``` r
Tab_S15 = data.frame(stringsAsFactors=FALSE,
   vehicle.type = c("Passenger Cars", "Public transport", "Bus", "Metro/Tram",
                    "Train", "Bicycle, on foot", "Passenger Cars",
                    "Public transport", "Bus", "Metro/Tram", "Train", "Bicycle, on foot",
                    "Passenger Cars", "Public transport", "Bus", "Metro/Tram",
                    "Train", "Bicycle, on foot"),
           Year = c(2005L, 2005L, 2005L, 2005L, 2005L, 2005L, 2010L, 2010L,
                    2010L, 2010L, 2010L, 2010L, 2015L, 2015L, 2015L, 2015L,
                    2015L, 2015L),
          percent = c(73L, 24L, 39L, 26L, 35L, 3L, 73L, 24L, 37L, 25L, 38L, 3L,
                    73L, 24L, 35L, 24L, 41L, 3L)
)
```

### Metadata

``` r
Metadata = data.frame(stringsAsFactors=FALSE,
   Austrian_HCfootprint_SIdata_for = c("Carbon emission trends and sustainability options in Austrian health care","",
                                       "Ulli Weisz (a), Peter-Paul Pichler (b), Ingram S. Jaccard (b), Willi Haas (a), Sarah Matej (a), Florian Bachner (c),  Peter Nowak (c),and Helga Weisz (b,d)","","(a) Institute of Social Ecology, University of Natural Resources and Life Sciences Vienna, Schottenfeldgasse 29, 1070 Vienna, Austria;",
                                       "(b) Social Metabolism and Impacts, Potsdam Institute for Climate Impact Research, Member of the Leibniz Association, PO Box 60 12 03, D-14412, Potsdam, Germany;", "(c) Austrian Public Health Institute (Gesundheit Österreich GmbH), Stubenring 6, 1010 Vienna, Austria;",
                                       "(d) Department of Cultural History & Theory and Department of Social Sciences, Humboldt University Berlin, Unter den Linden 6, D-10117, Berlin, Germany;")
)
```

# Write .xlsx data file

``` r
wb = createWorkbook()

addWorksheet(wb, "Metadata")
addWorksheet(wb, "HCF_summary")
addWorksheet(wb, "Fig1a_data")
addWorksheet(wb, "Fig1b_data")
addWorksheet(wb, "Fig2_data")
addWorksheet(wb, "Fig3_data")
addWorksheet(wb, "Fig4_data")
addWorksheet(wb, "Tab1_data")
addWorksheet(wb, "Tab2_data")
addWorksheet(wb, "Tab3_data")
addWorksheet(wb, "Tab_S2")
addWorksheet(wb, "Tab_S3")
addWorksheet(wb, "FigS1_data")
addWorksheet(wb, "Tab_S4")
addWorksheet(wb, "Tab_S5")
addWorksheet(wb, "Tab_S6")
addWorksheet(wb, "Tab_S10")
addWorksheet(wb, "Tab_S15")


writeData(wb, "Metadata", Metadata)
writeData(wb, "HCF_summary", HCF_summary)
writeData(wb, "Fig1a_data", Fig1a_data)
writeData(wb, "Fig1b_data", Fig1b_data)
writeData(wb, "Fig2_data", Fig2_data)
writeData(wb, "Fig3_data", Fig3_data)
writeData(wb, "Fig4_data", Fig4_data)
writeData(wb, "Tab1_data", Tab1_data)
writeData(wb, "Tab2_data", Tab2_data)
writeData(wb, "Tab3_data", Tab3_data)
writeData(wb, "Tab_S2", Tab_S2)
writeData(wb, "Tab_S3", Tab_S3)
writeData(wb, "FigS1_data", FigS1_data)
writeData(wb, "Tab_S4", Tab_S4)
writeData(wb, "Tab_S5", Tab_S5)
writeData(wb, "Tab_S6", Tab_S6)
writeData(wb, "Tab_S10", Tab_S10)
writeData(wb, "Tab_S15", Tab_S15)


saveWorkbook(wb, file = paste0(here(), "/data/", "AUTHCF_SI_data.xlsx"), overwrite = TRUE)
```

# Session information

``` r
sessionInfo()
```

    ## R version 3.6.2 (2019-12-12)
    ## Platform: x86_64-pc-linux-gnu (64-bit)
    ## Running under: Ubuntu 19.10
    ## 
    ## Matrix products: default
    ## BLAS:   /usr/lib/x86_64-linux-gnu/blas/libblas.so.3.8.0
    ## LAPACK: /usr/lib/x86_64-linux-gnu/lapack/liblapack.so.3.8.0
    ## 
    ## locale:
    ##  [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C              
    ##  [3] LC_TIME=de_DE.UTF-8        LC_COLLATE=en_US.UTF-8    
    ##  [5] LC_MONETARY=de_DE.UTF-8    LC_MESSAGES=en_US.UTF-8   
    ##  [7] LC_PAPER=de_DE.UTF-8       LC_NAME=C                 
    ##  [9] LC_ADDRESS=C               LC_TELEPHONE=C            
    ## [11] LC_MEASUREMENT=de_DE.UTF-8 LC_IDENTIFICATION=C       
    ## 
    ## attached base packages:
    ## [1] stats     graphics  grDevices utils     datasets  methods   base     
    ## 
    ## other attached packages:
    ##  [1] forcats_0.5.0   stringr_1.4.0   dplyr_0.8.5     purrr_0.3.3    
    ##  [5] readr_1.3.1     tidyr_1.0.2     tibble_2.1.3    ggplot2_3.3.0  
    ##  [9] tidyverse_1.3.0 openxlsx_4.1.4  wbstats_0.2     here_0.1       
    ## [13] readxl_1.3.1    pacman_0.5.1   
    ## 
    ## loaded via a namespace (and not attached):
    ##  [1] tidyselect_0.2.5 xfun_0.12        haven_2.2.0      lattice_0.20-38 
    ##  [5] colorspace_1.4-1 vctrs_0.2.3      generics_0.0.2   htmltools_0.4.0 
    ##  [9] yaml_2.2.0       rlang_0.4.5      pillar_1.4.3     withr_2.1.2     
    ## [13] glue_1.3.1       DBI_1.0.0        dbplyr_1.4.2     modelr_0.1.6    
    ## [17] lifecycle_0.2.0  munsell_0.5.0    gtable_0.3.0     cellranger_1.1.0
    ## [21] rvest_0.3.5      zip_2.0.4        evaluate_0.14    knitr_1.28      
    ## [25] curl_4.3         fansi_0.4.0      broom_0.5.5      Rcpp_1.0.3      
    ## [29] backports_1.1.5  scales_1.0.0     jsonlite_1.6     fs_1.3.2        
    ## [33] hms_0.5.3        digest_0.6.25    stringi_1.4.6    grid_3.6.2      
    ## [37] rprojroot_1.3-2  cli_2.0.2        tools_3.6.2      magrittr_1.5    
    ## [41] crayon_1.3.4     pkgconfig_2.0.3  ellipsis_0.3.0   xml2_1.2.2      
    ## [45] reprex_0.3.0     lubridate_1.7.4  rstudioapi_0.11  assertthat_0.2.1
    ## [49] rmarkdown_2.1    httr_1.4.1       R6_2.4.0         nlme_3.1-143    
    ## [53] compiler_3.6.2
