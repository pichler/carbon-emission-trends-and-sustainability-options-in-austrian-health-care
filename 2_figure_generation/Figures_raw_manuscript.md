This file generates the four figures found in the main manuscript. It
generates the raw versions of the figures. Colors and labels of the
figures were subsequently edited manually in Inkscape. Scales, data or
any other relevant aspects of data presentation have not been modified.

# Load or install external packages and data file

``` r
knitr::opts_chunk$set(echo = TRUE, message = FALSE)
if (!require("pacman")) install.packages("pacman")
pacman::p_load(readxl,
               scales,
               gridExtra,
               moonBook,
               webr,
               tidyverse,
               Cairo,
               here)
               

file_results = normalizePath(paste0(here(),"/data/","AUTHCF_SI_data.xlsx"))
```

# Figure 1

``` r
pdat1 = read_excel(file_results, sheet = "Fig1a_data") %>%
  filter(Year != 2015) %>%
  mutate(Provider = fct_relevel(Provider, 
                              "Investment",
                              "Retail",
                              "Other",
                              "Ambulatory", 
                              "Hospitals")) 


cols = c("#008280","#631879","#008b45","#ee0000","#3b4992")

p1 = ggplot(pdat1, aes(x=Year, y=ktCO2*0.001, group=Provider, fill=Provider)) +
  geom_area() +
  #scale_colour_manual(values = rev(pal_aaas(5))) +
  scale_fill_manual(values = cols) +
  labs(x="", y="Health carbon footprint (MtCO2)") +
  scale_x_continuous(breaks=c(2005, 2010, 2014)) +
  theme_minimal() +
  theme(legend.position = "bottom")+
  NULL

pdat2 = read_excel(file_results, sheet = "Fig1b_data") %>%
  filter(Year != 2015)

pdat2_points = pdat2 %>% filter(Year %in% c(2005, 2010, 2014))


p2 = ggplot(pdat2, aes(x=Year, y=expenditure)) +
  geom_line(aes(linetype=indicator)) +
  geom_point(data=pdat2_points) +
  labs(x="", y="Health Expenditure (constant prices)") +
  scale_x_continuous(breaks=c(2005, 2010, 2014)) +
  scale_linetype_manual(values = c(1,2), 
                        labels = c("EUR (2010 const)", "USD (2010 const)"), 
                        name="Health expenditure") +
  theme_minimal() +
  theme(legend.position = "bottom")+
  NULL

grid.arrange(p1,p2, nrow=1, widths = c(1,1))
```

<div class="figure" style="text-align: center">

<img src="Figures_raw_manuscript_files/figure-gfm/fig1-1.png" alt="Austrian health carbon footprint by provider (left) and health care expenditure from 2005 to 2014." width="60%" />

<p class="caption">

Austrian health carbon footprint by provider (left) and health care
expenditure from 2005 to 2014.

</p>

</div>

``` r
ggsave(paste0(here(),"/figures/","figure1_raw.pdf"), width = 240, height = 110, units = "mm",
    dpi = 300)
```

# Figure 2

Basic figure, colors and induced travel added manually outside in
inkscape, block therefore not evaluated here.

``` r
dat_figure2 = read_excel(file_results, sheet = "Fig2_data") %>%
  filter(Year == 2010) 

CairoPDF(paste0(here(),"/figures/","figure2_raw.pdf"), 7, 7, bg="transparent")
PieDonut(dat_figure2,aes(Provider,Category, count=ktCO2),start=-pi/2,labelposition=1)
```

<div class="figure" style="text-align: center">

<img src="Figures_raw_manuscript_files/figure-gfm/fig2-1.png" alt="CO2 footprint of the Austrian health care sector in 2010 divided into SHA categories for health care providers and investments (inner ring) with minor categories summarized into Other (section 2.1). In the categories Ambulatory and Hospitals (outer ring), emissions from direct energy consumption and induced travel (outer line) are also reported (sections 2.2 and 2.5). The category Hospitals was further subdivided by consumed goods and services (section 2.3)" width="70%" />

<p class="caption">

CO2 footprint of the Austrian health care sector in 2010 divided into
SHA categories for health care providers and investments (inner ring)
with minor categories summarized into Other (section 2.1). In the
categories Ambulatory and Hospitals (outer ring), emissions from direct
energy consumption and induced travel (outer line) are also reported
(sections 2.2 and 2.5). The category Hospitals was further subdivided by
consumed goods and services (section 2.3)

</p>

</div>

``` r
dev.off()
```

    ## png 
    ##   2

# Figure 3

``` r
dat_figure3 = read_excel(file_results, sheet = "Fig3_data") %>%
  mutate(Provider = fct_relevel(Provider, 
                                "Patient Transportation",
                                "Home health care services",
                                "Ambulatory", 
                                "Hospitals"))
  
cols = c("#2ce286ff", "#f3ccccff","#fb9595ff","#fb6060ff","#ee0000ff","#bcc3e4ff","#7484c7ff","#3b4992ff")
labs = c("Ambulance & patient transportation (diesel and petrol)",
         "Mobile home health services (diesel and petrol)",
         "Ambulatory health services (district heat)",
         "Ambulatory health services (electricity)",
         "Ambulatory health services (oil and gas)",
         "Hospitals (district heat)",
         "Hospitals (electricity)",
         "Hospitals (diesel, petrol, oil and gas)")

ggplot(dat_figure3, aes(x=as.factor(Year), y=ktCO2, fill=interaction(Energy.Type,Provider), group=Provider)) +
  geom_col(color="white") +
  #scale_fill_manual(values = c("#bb240e", "#cfe8e6", "#6fc223", "#ffffff")) +
  scale_fill_manual(values = cols, labels=labs, name="Provider (energy type)") +
  scale_alpha_manual(values = c(0.4, 0.7, 1)) +
  labs(x="", y="Carbon Emissions (ktCO2)") +
  theme_minimal() +
  NULL
```

<div class="figure" style="text-align: center">

<img src="Figures_raw_manuscript_files/figure-gfm/fig3-1.png" alt="Energy consumption of major Austrian health care providers: CO2 emissions 2005, 2010 and 2015" width="70%" />

<p class="caption">

Energy consumption of major Austrian health care providers: CO2
emissions 2005, 2010 and 2015

</p>

</div>

``` r
ggsave(paste0(here(),"/figures/","figure3_raw.pdf"), width = 135, height = 100, units = "mm",
    dpi = 300)
```

# Figure 4

``` r
dat_figure4 = read_excel(file_results, sheet = "Fig4_data") 
  
cols = c("#ee0000ff","#fb6060ff","#3b4992ff","#7484c7ff","#bcc3e4ff")
labs = c("Ambulatory health services (patients)",
         "Ambulatory health services (staff)",
         "Hospitals (patients)",
         "Hospitals (staff)",
         "Hospitals (visitors)")

ggplot(dat_figure4, aes(x=as.factor(Year), y=ktCO2, fill=interaction(Category,Provider), group=Provider)) +
  geom_col(color="white") +
  #scale_fill_manual(values = c("#bb240e", "#cfe8e6", "#6fc223", "#ffffff")) +
  scale_fill_manual(values = cols, labels=labs, name="Provider (group)") +
  scale_alpha_manual(values = c(0.4, 0.7, 1)) +
  guides(fill = guide_legend(reverse = TRUE)) +
  labs(x="", y="Carbon Emissions (ktCO2)") +
  theme_minimal() +
  NULL
```

<div class="figure" style="text-align: center">

<img src="Figures_raw_manuscript_files/figure-gfm/fig4-1.png" alt="health care sector induced travel by hospitals and ambulatory health care: CO2 emissions 2005, 2010 and 2015" width="60%" />

<p class="caption">

health care sector induced travel by hospitals and ambulatory health
care: CO2 emissions 2005, 2010 and 2015

</p>

</div>

``` r
ggsave(paste0(here(),"/figures/","figure4_raw.pdf"), width = 135, height = 100, units = "mm",
    dpi = 300)
```

# Session information

``` r
sessionInfo()
```

    ## R version 3.6.2 (2019-12-12)
    ## Platform: x86_64-pc-linux-gnu (64-bit)
    ## Running under: Ubuntu 19.10
    ## 
    ## Matrix products: default
    ## BLAS:   /usr/lib/x86_64-linux-gnu/blas/libblas.so.3.8.0
    ## LAPACK: /usr/lib/x86_64-linux-gnu/lapack/liblapack.so.3.8.0
    ## 
    ## locale:
    ##  [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C              
    ##  [3] LC_TIME=de_DE.UTF-8        LC_COLLATE=en_US.UTF-8    
    ##  [5] LC_MONETARY=de_DE.UTF-8    LC_MESSAGES=en_US.UTF-8   
    ##  [7] LC_PAPER=de_DE.UTF-8       LC_NAME=C                 
    ##  [9] LC_ADDRESS=C               LC_TELEPHONE=C            
    ## [11] LC_MEASUREMENT=de_DE.UTF-8 LC_IDENTIFICATION=C       
    ## 
    ## attached base packages:
    ## [1] stats     graphics  grDevices utils     datasets  methods   base     
    ## 
    ## other attached packages:
    ##  [1] here_0.1        Cairo_1.5-11    forcats_0.5.0   stringr_1.4.0  
    ##  [5] dplyr_0.8.5     purrr_0.3.3     readr_1.3.1     tidyr_1.0.2    
    ##  [9] tibble_2.1.3    ggplot2_3.3.0   tidyverse_1.3.0 webr_0.1.5     
    ## [13] moonBook_0.2.3  gridExtra_2.3   scales_1.0.0    readxl_1.3.1   
    ## [17] pacman_0.5.1   
    ## 
    ## loaded via a namespace (and not attached):
    ##  [1] nlme_3.1-143       fs_1.3.2           lubridate_1.7.4    insight_0.8.2     
    ##  [5] RColorBrewer_1.1-2 httr_1.4.1         rprojroot_1.3-2    tools_3.6.2       
    ##  [9] backports_1.1.5    R6_2.4.0           DT_0.12            sjlabelled_1.1.3  
    ## [13] DBI_1.0.0          colorspace_1.4-1   withr_2.1.2        tidyselect_0.2.5  
    ## [17] mnormt_1.5-6       compiler_3.6.2     cli_2.0.2          rvest_0.3.5       
    ## [21] flextable_0.5.9    xml2_1.2.2         officer_0.3.7      labeling_0.3      
    ## [25] lmtest_0.9-37      psych_1.9.12.31    systemfonts_0.1.1  digest_0.6.25     
    ## [29] editData_0.1.2     rmarkdown_2.1      base64enc_0.1-3    pkgconfig_2.0.3   
    ## [33] htmltools_0.4.0    highr_0.8          dbplyr_1.4.2       fastmap_1.0.1     
    ## [37] rvg_0.2.4          htmlwidgets_1.3    rlang_0.4.5        rstudioapi_0.11   
    ## [41] shiny_1.4.0        farver_2.0.3       generics_0.0.2     zoo_1.8-7         
    ## [45] jsonlite_1.6       zip_2.0.4          magrittr_1.5       fansi_0.4.0       
    ## [49] Rcpp_1.0.3         munsell_0.5.0      gdtools_0.2.1      lifecycle_0.2.0   
    ## [53] stringi_1.4.6      yaml_2.2.0         snakecase_0.11.0   MASS_7.3-51.5     
    ## [57] grid_3.6.2         parallel_3.6.2     promises_1.1.0     sjmisc_2.8.3      
    ## [61] crayon_1.3.4       miniUI_0.1.1.1     lattice_0.20-38    haven_2.2.0       
    ## [65] hms_0.5.3          knitr_1.28         pillar_1.4.3       uuid_0.1-4        
    ## [69] reprex_0.3.0       glue_1.3.1         evaluate_0.14      data.table_1.12.2 
    ## [73] modelr_0.1.6       vcd_1.4-6          vctrs_0.2.3        tweenr_1.0.1      
    ## [77] httpuv_1.5.2       cellranger_1.1.0   gtable_0.3.0       polyclip_1.10-0   
    ## [81] assertthat_0.2.1   xfun_0.12          ggforce_0.3.1      mime_0.9          
    ## [85] xtable_1.8-4       broom_0.5.5        later_1.0.0        rrtable_0.1.7     
    ## [89] ellipsis_0.3.0     devEMF_3.7         ztable_0.2.0
