# Carbon emission trends and sustainability options in Austrian health care

Companion repository for the publication [Carbon emission trends and sustainability options in Austrian health care](https://www.sciencedirect.com/science/article/abs/pii/S0921344920301828)

This repository contains all code and documentation to reproduce the:

1) [Preprocessing and analysis](1_preprocessing_analysis/Preprocessing_analysis.md) to generate the SI data file [AUTHCF_SI_data.xlsx](data/AUTHCF_SI_data.xlsx) that includes all results, figures and table data
2) generation of the [raw figures](2_figure_generation/Figures_raw_manuscript.md) used in the manuscript
3) [text and results](3_manuscript/Manuscript_for_md.md) reported in the manuscript itself
4) [EE-MRIO health carbon footprint results](code/eemrio_hcf.R) reported in the manuscript

## Licensing

- [![License: CC BY-NC-ND 4.0](https://licensebuttons.net/l/by-nc-nd/4.0/80x15.png)](https://creativecommons.org/licenses/by-nc-nd/4.0/): manuscript (contents of the `3_manuscript` folder)
- [![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0): all other contents of this repository
